#include "mfrc522.h"
#include "config.h"
#include "gpiox.h"

static MFRC_CFG_T *cfg;

void mfrc522_init(MFRC_CFG_T *c){
    uint8_t byte;
    //DEBUGSTR("Initialize MFRC522 chip!\n");
    cfg = c;
    if (!cfg->reset) cfg->reset();

    mfrc522_reset();

    mfrc522_write(TModeReg, 0x8D);
    mfrc522_write(TPrescalerReg, 0x3E);
    mfrc522_write(TReloadReg_1, 30);
    mfrc522_write(TReloadReg_2, 0);
    mfrc522_write(TxASKReg, 0x40);
    mfrc522_write(ModeReg, 0x3D);

    byte = mfrc522_read(TxControlReg);
    if(!(byte&0x03)){
        mfrc522_write(TxControlReg,byte|0x03);
    }
}

void mfrc522_write(uint8_t reg, uint8_t data){
    cfg->assert();
    cfg->spi_txrx((reg<<1)&0x7E);
    cfg->spi_txrx(data);
    cfg->deassert();
}

uint8_t mfrc522_read(uint8_t reg){
    uint8_t data;
    cfg->assert();
    cfg->spi_txrx(((reg<<1)&0x7E)|0x80);
    data = cfg->spi_txrx(0x00);
    cfg->deassert();
    return data;
}

void mfrc522_reset(){
    mfrc522_write(CommandReg, SoftReset_CMD);
}

uint8_t mfrc522_request(uint8_t req_mode, uint8_t * tag_type){
    uint8_t  status;
    uint32_t backBits;//The received data bits

    mfrc522_write(BitFramingReg, 0x07);//TxLastBists = BitFramingReg[2..0]  ???

    tag_type[0] = req_mode;
    status = mfrc522_to_card(Transceive_CMD, tag_type, 1, tag_type, &backBits);

    if ((status != CARD_FOUND) || (backBits != 0x10)){
        status = CARD_ERROR;
    }
    return status;
}

uint8_t mfrc522_to_card(uint8_t cmd, uint8_t *send_data, uint8_t send_data_len, uint8_t *back_data, uint32_t *back_data_len){
    uint8_t status = CARD_ERROR;
    uint8_t irqEn = 0x00;
    uint8_t waitIRq = 0x00;
    uint8_t lastBits;
    uint8_t n;
    uint8_t tmp;
    uint32_t i;

    switch (cmd){
        case MFAuthent_CMD:     //Certification cards close
        {
            irqEn = 0x12;
            waitIRq = 0x10;
            break;
        }
        case Transceive_CMD:    //Transmit FIFO data
        {
            irqEn = 0x77;
            waitIRq = 0x30;
            break;
        }
        default:
            break;
    }

    //mfrc522_write(ComIEnReg, irqEn|0x80); //Interrupt request
    n=mfrc522_read(ComIrqReg);
    mfrc522_write(ComIrqReg,n&(~0x80));//clear all interrupt bits
    n=mfrc522_read(FIFOLevelReg);
    mfrc522_write(FIFOLevelReg,n|0x80);//flush FIFO data

    mfrc522_write(CommandReg, Idle_CMD);    //NO action; Cancel the current cmd???

    //Writing data to the FIFO
    for (i=0; i<send_data_len; i++){
        mfrc522_write(FIFODataReg, send_data[i]);
    }

    //Execute the cmd
    mfrc522_write(CommandReg, cmd);
    if (cmd == Transceive_CMD){
        n=mfrc522_read(BitFramingReg);
        mfrc522_write(BitFramingReg,n|0x80);
    }

    //Waiting to receive data to complete
    i = 2000;   //i according to the clock frequency adjustment, the operator M1 card maximum waiting time 25ms???
    do{
        //CommIrqReg[7..0]
        //Set1 TxIRq RxIRq IdleIRq HiAlerIRq LoAlertIRq ErrIRq TimerIRq
        n = mfrc522_read(ComIrqReg);
        i--;
    }
    while ((i!=0) && !(n&0x01) && !(n&waitIRq));

    tmp=mfrc522_read(BitFramingReg);
    mfrc522_write(BitFramingReg,tmp&(~0x80));

    if (i != 0){
        if(!(mfrc522_read(ErrorReg) & 0x1B)){   //BufferOvfl Collerr CRCErr ProtecolErr
            status = CARD_FOUND;
            if (n & irqEn & 0x01){
                status = CARD_NOT_FOUND;            //??
            }

            if (cmd == Transceive_CMD){
                n = mfrc522_read(FIFOLevelReg);
                lastBits = mfrc522_read(ControlReg) & 0x07;
                if (lastBits){
                    *back_data_len = (n-1)*8 + lastBits;
                } else {
                    *back_data_len = n*8;
                }

                if (n == 0){
                    n = 1;
                }
                if (n > MAX_LEN){
                    n = MAX_LEN;
                }

                //Reading the received data in FIFO
                for (i=0; i<n; i++){
                    back_data[i] = mfrc522_read(FIFODataReg);
                }
            }
        } else {
            status = CARD_ERROR;
        }
    }
    //SetBitMask(ControlReg,0x80);           //timer stops
    //mfrc522_write(cmdReg, PCD_IDLE);
    return status;
}

uint8_t mfrc522_get_card_serial(uint8_t * serial_out){
    uint8_t status;
    uint8_t i;
    uint8_t serNumCheck=0;
    uint32_t unLen;

    mfrc522_write(BitFramingReg, 0x00);     //TxLastBists = BitFramingReg[2..0]

    serial_out[0] = PICC_ANTICOLL;
    serial_out[1] = 0x20;
    status = mfrc522_to_card(Transceive_CMD, serial_out, 2, serial_out, &unLen);

    if (status == CARD_FOUND){
        //Check card serial number
        for (i=0; i<4; i++){
            serNumCheck ^= serial_out[i];
        }
        if (serNumCheck != serial_out[i]){
            status = CARD_ERROR;
        }
    }
    return status;
}

static inline void mfrc522_assert(void){
    GPIO_CLR(MFRC522_GPIO_SEL);
}

static inline void mfrc522_deassert(void){
    GPIO_SET(MFRC522_GPIO_SEL);
}

#include "board.h"
static inline void mfrc522_test(void){

    MFRC_CFG_T testcfg;
    testcfg.assert = mfrc522_assert;
    testcfg.deassert = mfrc522_deassert;

    mfrc522_init(&testcfg);
    uint8_t byte;
    byte = mfrc522_read(VersionReg);

    if(byte == 0x92){
        DEBUGSTR("MIFARE RC522v2 Detected!\n");
    } else if(byte == 0x91 || byte==0x90) {
        DEBUGSTR("MIFARE RC522v1 Detected!\n");
    } else {
        DEBUGOUT("No reader found. DATA: %0x\n", byte);
    }
    uint8_t str[16];

    while(1){
        byte = mfrc522_request(PICC_REQALL,str);
        if(byte == CARD_FOUND){
            DEBUGSTR("CARD FOUND 1\n");
            byte = mfrc522_get_card_serial(str);
            if(byte == CARD_FOUND){
                DEBUGOUT("CARD FOUND 2\n");
//                dump_packet(str,16);
            }else{
                DEBUGOUT("WTF: %x \n", byte);
            }
        }
    }
}
