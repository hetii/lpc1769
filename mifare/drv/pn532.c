#include "gpiox.h"
#include "pn532.h"
#include "config.h"
#include "board.h"
#include "timer.h"
#include "softspi.h"

static void rfid_tx (unsigned char data){
    softspi_txrx_byte(data);
    /*spi_txrx ((SPI_CS_PN532 ^ SPI_CS_MODE_SKIP_TX) |
              SPI_CS_MODE_SKIP_CS_ASSERT |
              SPI_CS_MODE_SKIP_CS_DEASSERT, &data, sizeof (data), NULL, 0);*/
}

static unsigned char rfid_rx (void){
    unsigned char data;
    /*
    spi_txrx ((SPI_CS_PN532 ^ SPI_CS_MODE_SKIP_TX) |
              SPI_CS_MODE_SKIP_CS_ASSERT |
              SPI_CS_MODE_SKIP_CS_DEASSERT, NULL, 0, &data, sizeof (data));*/
    data = softspi_txrx_byte(data);
    return data;
}

int rfid_read (void *data, unsigned char size){
    int res;
    unsigned char *p, c, pkt_size, crc, prev, t;

    /* wait 100ms max till PN532 response is ready */
    t = 0;
    while (GPIO_GET(PN532_GPIO_IRQ)){

        if (t++ > 10)
            return -8;
        delay_ms (10);
    }

    DEBUGSTR("RI: ");
    GPIO_CLR(PN532_GPIO_SEL);

    /* read from FIFO command */
    rfid_tx (0x03);

    /* default result */
    res = -9;

    /* find preamble */
    t = 0;
    prev = rfid_rx ();
    while ((!(((c = rfid_rx ()) == 0xFF) && (prev == 0x00))) && (t < PN532_FIFO_SIZE)){
        prev = c;
        t++;
    }

    if (t >= PN532_FIFO_SIZE)
        res = -3;
    else
    {
        /* read packet size */
        pkt_size = rfid_rx ();

        /* special treatment for NACK and ACK */
        if ((pkt_size == 0x00) || (pkt_size == 0xFF))
        {
            /* verify if second length byte is inverted */
            if (rfid_rx () != (unsigned char) (~pkt_size))
                res = -2;
            else
            {
                /* eat Postamble */
                rfid_rx();
                /* -1 for NACK, 0 for ACK */
                res = pkt_size ? -1 : 0;
            }
        }
        else
        {
            /* verify packet size against LCS */
            if (((pkt_size + rfid_rx ()) & 0xFF) != 0)
                res = -4;
            else
            {
                /* remove TFI from packet size */
                pkt_size--;
                /* verify if packet fits into buffer */
                if (pkt_size > size)
                    res = -5;
                else
                {
                    /* remember actual packet size */
                    size = pkt_size;
                    /* verify TFI */
                    if ((crc = rfid_rx ()) != 0xD5)
                        res = -6;
                    else
                    {
                        /* read packet */
                        p = (unsigned char *) data;
                        while (pkt_size--)
                        {
                            /* read data */
                            c = rfid_rx ();
                            DEBUGOUT(" %02X", c);

                            /* maintain crc */
                            crc += c;
                            /* save payload */
                            if (p)
                                *p++ = c;
                        }

                        /* add DCS to CRC */
                        crc += rfid_rx ();
                        /* verify CRC */
                        if (crc)
                            res = -7;
                        else
                        {
                            /* eat Postamble */
                            rfid_rx ();
                            /* return actual size as result */
                            res = size;
                        }
                    }
                }
            }
        }
    }
    GPIO_SET(PN532_GPIO_SEL);
    DEBUGOUT(" [%i]\n", res);
    /* everything fine */
    return res;
}

int rfid_write (const void *data, int len){
    int i;
    static const unsigned char preamble[] = { 0x01, 0x00, 0x00, 0xFF };
    const unsigned char *p = preamble;
    unsigned char tfi = 0xD4, c;

    if (!data)
        len = 0xFF;

    DEBUGSTR("TI: ");
    GPIO_CLR(PN532_GPIO_SEL);

    p = preamble;                                                               /* Praeamble */
    for (i = 0; i < (int) sizeof (preamble); i++)
        rfid_tx (*p++);
    rfid_tx (len + 1);                                                          /* LEN */
    rfid_tx (0x100 - (len + 1));                                                /* LCS */
    rfid_tx (tfi);                                                              /* TFI */
    /* PDn */
    p = (const unsigned char *) data;
    while (len--)
    {
        c = *p++;
        DEBUGOUT(" %02X", c);

        rfid_tx (c);
        tfi += c;
    }
    rfid_tx (0x100 - tfi);                                                      /* DCS */
    rfid_rx ();                                                                 /* Postamble */

    /* release chip select */
    GPIO_SET(PN532_GPIO_SEL);
    DEBUGSTR("\n");
    /* check for ack */
    return rfid_read (NULL, 0);
}

int rfid_execute (void *data, unsigned int isize, unsigned int osize){
    int res;

    if ((res = rfid_write (data, isize)) < 0)
        return res;
    else
        return rfid_read (data, osize);
}

int rfid_write_register (unsigned short address, unsigned char data){
    unsigned char cmd[4];

    /* write register */
    cmd[0] = PN532_CMD_WriteRegister;
    /* high byte of address */
    cmd[1] = address >> 8;
    /* low byte of address */
    cmd[2] = address & 0xFF;
    /* data value */
    cmd[3] = data;

    return rfid_execute (&cmd, sizeof (cmd), sizeof (data));
}

int rfid_read_register (unsigned short address){
    int res;
    unsigned char cmd[3];

    /* write register */
    cmd[0] = PN532_CMD_ReadRegister;
    /* high byte of address */
    cmd[1] = address >> 8;
    /* low byte of address */
    cmd[2] = address & 0xFF;

    if ((res = rfid_execute (&cmd, sizeof (cmd), sizeof (cmd))) > 1)
        return cmd[1];
    else
        return res;
}

int rfid_mask_register (unsigned short address, unsigned char data, unsigned char mask){
    int res;

    if ((res = rfid_read_register (address)) < 0)
        return res;
    else
        return rfid_write_register (address,
                                    (((unsigned char) res) & (~mask)) | (data
                                                                         &
                                                                         mask));
}

void rfid_init (void){
    GPIO_MUX(PN532_GPIO_IRQ);
    GPIO_MUX(PN532_GPIO_RST);
    GPIO_MUX(PN532_GPIO_SEL);

    GPIO_OUT(PN532_GPIO_RST);
    GPIO_OUT(PN532_GPIO_SEL);
    GPIO_INP(PN532_GPIO_IRQ);

    GPIO_CLR(PN532_GPIO_RST);
    delay_ms(400);
    GPIO_SET(PN532_GPIO_RST);
    /* wait for PN532 to boot */
    delay_ms (100);
}
