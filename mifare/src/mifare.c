#include "board.h"
#include "gpiox.h"
#include "mfrc522.h"
#include "softspi.h"
#include "hardspi.h"
#include "timer.h"

void dump_packet (uint8_t * data, int count){
    int i;
    for (i = 0; i < count; i++)
        DEBUGOUT("%c%02X", i == 6 ? '*' : ' ', *data++);
    DEBUGSTR("\n");
}

int main(void) {
    SystemCoreClockUpdate();
    Board_Init();
    SysTickInit();
    openpcd();

    return 0 ;
}
