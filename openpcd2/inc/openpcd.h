/*
 * openpcd.h
 *
 *  Created on: 2 gru 2014
 *      Author: mint
 */

#ifndef OPENPCD_H_
#define OPENPCD_H_

#define PN532_FIFO_SIZE 64
#define PN532_MAX_PAYLAOADSIZE 264
#define PN532_MAX_PACKET_SIZE (PN532_MAX_PAYLAOADSIZE+11)

typedef enum
{
    STATE_IDLE = 0,
    STATE_PREFIX = -1,
    STATE_PREFIX_EXT = -2,
    STATE_HEADER = -3,
    STATE_WAKEUP = -4,
    STATE_FIFOFLUSH = -5,
    STATE_PAYLOAD = -6,
    STATE_FLOWCTRL = -7
} PN532_State;

typedef struct
{
    uint32_t last_seen;
    uint16_t reserved;
    uint16_t pos;
    uint16_t expected;
    uint8_t data_prev;
    uint8_t wakeup;
    uint8_t crc;
    uint8_t tfi;
    PN532_State state;
    uint8_t data[PN532_MAX_PACKET_SIZE + 1];
} PN532_Packet;

static PN532_Packet buffer_put, buffer_get;

#endif /* OPENPCD_H_ */
