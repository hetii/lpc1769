/*
 * spi.h
 *
 *  Created on: 4 gru 2014
 *      Author: mint
 */

#ifndef SPI_H_
#define SPI_H_


typedef uint32_t spi_cs;

#define SPI_CS_MODE_NORMAL 0
#define SPI_CS_MODE_INVERT_CS 1
#define SPI_CS_MODE_BIT_REVERSED 2
#define SPI_CS_MODE_SKIP_TX 4
#define SPI_CS_MODE_SKIP_CS_ASSERT 8
#define SPI_CS_MODE_SKIP_CS_DEASSERT 0x10
#define SPI_CS_MODE_LCD_CMD 0x20
#define SPI_CS_MODE_LCD_DAT 0x40
#define SPI_CS_MODE_LCD (SPI_CS_MODE_LCD_CMD|SPI_CS_MODE_LCD_DAT)

#define SPI_CS_MODE_SKIP_CS (SPI_CS_MODE_SKIP_CS_ASSERT|SPI_CS_MODE_SKIP_CS_DEASSERT)
#define SPI_CS(port,pin,CPSDVSR,mode) ((spi_cs)( ((((uint32_t)port)&0xFF)<<24) | ((((uint32_t)pin)&0xFF)<<16) | ((((uint32_t)CPSDVSR)&0xFF)<<8) | (((uint32_t)mode)&0xFF) ))

#define SPI_CS_PN532 SPI_CS( PN532_CS_PORT, PN532_CS_PIN, 64, SPI_CS_MODE_SKIP_TX|SPI_CS_MODE_BIT_REVERSED )

extern void spi_init (void);
extern void spi_close (void);
extern void spi_status (void);
extern void spi_init_pin (spi_cs chipselect);
extern void spi_txrx_done (spi_cs chipselect);
extern int spi_txrx (spi_cs chipselect, const void *tx, uint16_t txlen, void *rx, uint16_t rxlen);

#endif /* SPI_H_ */
