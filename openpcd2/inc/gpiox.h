
#ifndef DRV_GPIO_H_
#define DRV_GPIO_H_

#include "board.h"

extern LPC_GPIO_T *GPIO_GetPointer(uint8_t portNum);
extern inline bool GPIOGetValue (uint8_t portNum, uint8_t portPin);

void GPIOSetValue(uint8_t portNum, uint8_t portPin, uint8_t value);
void GPIOToogleXtimes(uint8_t portNum, uint8_t portPin, uint8_t times);
void GPIOSetDir(uint32_t portNum, uint32_t bitPosi, uint32_t dir);


#endif
