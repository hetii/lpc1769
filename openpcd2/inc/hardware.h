/*
 * hardware.h
 *
 *  Created on: 2 gru 2014
 *      Author: mint
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

#define PN532_RESET_PORT 1
#define PN532_RESET_PIN 11
#define PN532_IRQ_PORT 1
#define PN532_IRQ_PIN 4
#define PN532_CS_PORT 0
#define PN532_CS_PIN 2

#endif /* HARDWARE_H_ */
