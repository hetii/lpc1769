/*
 * timer.h
 *
 *  Created on: 6 gru 2014
 *      Author: mint
 */

#ifndef TIMER_H_
#define TIMER_H_

volatile uint32_t msTicks; // counter for 1ms SysTicks

// ms_delay - creates a delay of the appropriate number of Systicks (happens every 1 ms)
__INLINE static void delay_ms (uint32_t delayTicks) {
  uint32_t currentTicks;

  currentTicks = msTicks;   // read current tick counter
  // Now loop until required number of ticks passes.
  while ((msTicks - currentTicks) < delayTicks)
  {
      __WFI();
  }
}

extern void SysTickInit(void);


typedef struct
{
  void (*delayUs)(uint32_t);
  void (*delayMs)(uint32_t);
  bool (*enabled)(void);
  void (*startMs)(uint32_t);
  void (*stop)(void);
  bool (*expired)(void);
} timer_t;

extern const timer_t g_timer32b0m0;
extern const timer_t g_timer32b0m1;

#endif /* TIMER_H_ */
