/*
 * spi.c
 *
 *  Created on: 4 gru 2014
 *      Author: mint
 */
#include "board.h"
static SPI_CONFIG_FORMAT_T spi_format;
#define BUFFER_SIZE (0x100)

static uint8_t spi_tx_buf[BUFFER_SIZE];
static uint8_t spi_rx_buf[BUFFER_SIZE];

static SPI_CONFIG_FORMAT_T spi_format;
static SPI_DATA_SETUP_T spi_xf;

void spi_init (void){
    Board_SPI_Init(true);
    /* SPI initialization */
    Chip_SPI_Init(LPC_SPI);
    spi_format.bits = SPI_BITS_8;
    spi_format.clockMode = SPI_CLOCK_MODE0;
    spi_format.dataOrder = SPI_DATA_MSB_FIRST;
    Chip_SPI_SetFormat(LPC_SPI, &spi_format);

    Chip_SPI_SetMode(LPC_SPI, SPI_MODE_MASTER);

    spi_xf.fnBefFrame = Board_SPI_AssertSSEL;
    spi_xf.fnAftFrame = Board_SPI_DeassertSSEL;
    spi_xf.fnBefTransfer = NULL;
    spi_xf.fnAftTransfer = NULL;

    spi_xf.cnt = 0;
    spi_xf.length = BUFFER_SIZE;
    spi_xf.pTxData = spi_tx_buf;
    spi_xf.pRxData = spi_rx_buf;
    //bufferInit(spi_tx_buf, spi_rx_buf);

    Chip_SPI_RWFrames_Blocking(LPC_SPI, &spi_xf);
}
