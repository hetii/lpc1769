#include "timer.h"
#include "board.h"

#define TICKRATE_HZ1 (11)   /* 11 ticks per second */

static volatile uint8_t sleeping;

//  SysTick_Handler - just increment SysTick counter
void SysTick_Handler(void) {
  msTicks++;
}

void SysTickInit(void){
    SystemCoreClockUpdate();
    if (SysTick_Config(SystemCoreClock / 1000)){
        while (1);
    }
}

void TIMER0_IRQHandler(void){
    if (Chip_TIMER_MatchPending(LPC_TIMER0, 1)) {
        sleeping = false;
        Chip_TIMER_ClearMatch(LPC_TIMER0, 1);
    }
}

static void Timer32b0m0DelayUs (uint32_t us) {

    uint32_t timerFreq;

    /* Generic Initialization */
    SystemCoreClockUpdate();
    Board_Init();

    /* Enable timer 1 clock */
    Chip_TIMER_Init(LPC_TIMER0);

    /* Timer rate is system clock rate */
    timerFreq = Chip_Clock_GetSystemClockRate();

    /* Timer setup for match and interrupt at TICKRATE_HZ */
    Chip_TIMER_Reset(LPC_TIMER0);                                   //Chip_Clock_EnablePeriphClock(Chip_Timer_GetClockIndex(pTMR));
    Chip_TIMER_MatchEnableInt(LPC_TIMER0, 1);                       //MCR
    Chip_TIMER_SetMatch(LPC_TIMER0, 1, (timerFreq / TICKRATE_HZ1)); //MR
    Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, 1);                   //MCR
    Chip_TIMER_Enable(LPC_TIMER0);                                  //TCR

    /* Enable timer interrupt */
    NVIC_ClearPendingIRQ(TIMER0_IRQn);
    NVIC_EnableIRQ(TIMER0_IRQn);
    //#define DELAYUS(A) __delay3ticks(A*(__SYSTEM_CLOCK/3000000));
}
static void Timer32b0m0DelayMs (uint32_t ms) { Timer32b0m0DelayUs(ms * 1000); }
static bool Timer32b0m0Enabled (void) {  return 1; }
static void Timer32b0m0StartMs (uint32_t ms) {  }
static void Timer32b0m0Stop    (void) {  }
static bool Timer32b0m0Expired (void) { return 1; }

const timerx_t g_timer32b0m0 =
{
  Timer32b0m0DelayUs,
  Timer32b0m0DelayMs,
  Timer32b0m0Enabled,
  Timer32b0m0StartMs,
  Timer32b0m0Stop,
  Timer32b0m0Expired
};
