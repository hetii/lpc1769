#ifndef DRV_HARDSPI_H_
#define DRV_HARDSPI_H_
#include <stdint.h>

uint16_t hardspi_txrx_byte(uint8_t data);
void hardspi_init(void);

#endif /* DRV_HARDSPI_H_ */
