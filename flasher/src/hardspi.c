#include "hardspi.h"
#include "board.h"

void hardspi_init(void){
    /* Set GPIO */
    static SPI_CONFIG_FORMAT_T spi_format;

    Board_SPI_Init(true);
    /* SPI initialization */
    Chip_SPI_Init(LPC_SPI);

    Chip_SPI_SetClockCounter(LPC_SPI, 8);
    spi_format.bits = SPI_BITS_8;
    spi_format.clockMode = SPI_CLOCK_MODE0;
    spi_format.dataOrder = SPI_DATA_MSB_FIRST;

    Chip_SPI_SetFormat(LPC_SPI, &spi_format);
    Chip_SPI_SetMode(LPC_SPI, SPI_MODE_MASTER);

}

uint16_t hardspi_txrx_byte(uint8_t data){
        uint32_t status;

        //Chip_SPI_Int_FlushData(LPC_SPI);
        Chip_SPI_Int_ClearStatus(LPC_SPI, SPI_INT_SPIF);

        Chip_SPI_SendFrame(LPC_SPI, data);
        /* Wait for transfer completes */
        while (1){
            status = Chip_SPI_GetStatus(LPC_SPI);
            /* Check error */
            if (status & SPI_SR_ERROR){
                DEBUGSTR("Error occurred in SPI transmission!\n");
                return 0;
            }
            if (status & SPI_SR_SPIF){
                break;
            }
        }
        return Chip_SPI_ReceiveFrame(LPC_SPI);
}
