#include "board.h"
#include <stdlib.h>
#include <string.h>
#include "programmer.h"
#include "timer.h"
#include "cdc_vcom.h"

static PRG_CFG_T *cfg;

enum BUFFSIZE {
	OUT_BUFF_SIZE = USB_FS_MAX_BULK_PACKET
};

enum FLASHCMDS {
    WREN     = 0x06,    // WRite ENable
    WRDI     = 0x04,    // WRite DIsable

    EWSR     = 0x50,    // Enable Write Status Register
    WRSR     = 0x01,    // WRite Status Register
    RDSR     = 0x05,    // ReaD Status Register
    RDCR     = 0x15,    // ReaD Configuration Register
    RDSCUR   = 0x2b,    // ReaD SeCUrity Register (Macronix)
    WRSCUR   = 0x2f,    // WRite SeCUrity Register (Macronix)

    READ     = 0x03,    // READ data
    FASTREAD = 0x0b,    // FAST READ data
    READ2NX  = 0x3b,    // READ data with 2 IO (Numonyx)
    READ2MX  = 0xbb,    // READ data with 2 IO (Macronix)
    READ4MX  = 0xeb,    // READ data with 4 IO, 6/configurable dummy cycles (Macronix)
    READ44MX = 0xe7,    // READ data with 4 IO, 4 dummy cycles (Macronix)

    RRE00    = 0x00,    // Release Read Enhanced (Macronix)
    RRE55    = 0x55,    // Release Read Enhanced (Macronix)
    RREAA    = 0xaa,    // Release Read Enhanced (Macronix)
    RREFF    = 0xff,    // Release Read Enhanced (Macronix)

    PP       = 0x02,    // Page Program
    PW       = 0x0a,    // Page Write
    PP2      = 0xa2,    // Page Program with 2 IO
    PP4      = 0x38,    // Page Program with 4 IO
    AAIPA    = 0xaf,    // Auto Address Increment Program for SST *A
    AAIPB    = 0xad,    // Auto Address Increment Program for SST *B

    PE       = 0xdb,    // Page Erase
    PEAT     = 0x81,    // Atmel Page Erase
    SE       = 0x20,    // Sector Erase (4kB)
    BE32     = 0x52,    // Block Erase (32kB)
    BE64     = 0xd8,    // Block Erase (64kB)
    BE128    = 0x7c,    // Block Erase (64kB)
    CE       = 0xc7,    // Chip Erase
    CE_ALT   = 0x60,    // alternate Chip Erase

    RDP      = 0xab,    // Release from Deep Power down
    RES      = 0xab,    // release from deep power down and Read Electronic Signature
    REMS     = 0x90,    // release from deep power downSTR  and Read Electronic Manufacturer Signature
    REMS2    = 0xef,    // release from deep power down and Read Electronic Manufacturer Signature (Macronix)
    REMS4    = 0xdf,    // release from deep power down and Read Electronic Manufacturer Signature (Macronix)
    RDID     = 0x9f,    // ReaD ID
    QPIID    = 0xaf,    // Quad ReaD ID (Macronix)
    RDIDS    = 0x9e,    // Short ReaD ID
    RDSFDP   = 0x5a,    // JESD-216 ReaD Serial Flash Discoverable Parameter
    RDIDA    = 0x15,    // ReaD ID (old Atmel)

    ENSO     = 0xb1,    // Enter Secured OTP (Macronix)
    EXSO     = 0xc1,    // Exit Secured OTP (Macronix)

    DP       = 0xb9,    // Deep Power down
    UDP      = 0x79,    // Ultra Deep Power down

    RSTEN    = 0x66,    // ReSeT ENable
    RST      = 0x99,    // ReSeT

    NOP      = 0x00     // No OPeration
};

/*
 * Atmel AT45DB    + RES, RDID               +  fast read/write             + PE/SE/BE128/CE (4byte  CE command)
 * Atmel AT45DB    + RDID                    +  fast read/write             + PE/SE/BE128/CE (4byte  CE command)
 * Numonyx M25P    + RES, RDID (some)        +  fast read/write             + BE64/CE
 * Numonyx M25PX   + RDID                    +  fast/double read/write      + SE/BE64/CE
 * Numonyx M25PE   + RDID                    +  fast/double read/write      + SE/BE64/CE
 * Macronix MX25U  + RES, REMS, RDID, RDSFDP +  fast/double/quad read/write + SE/BE32/BE64/CE/CE_ALT
 * SST 25VF*       + RES, REMS               +  fast read/write (Byte/AAIP) + SE/BE32/CE/CE_ALT
 * SST 25VF*A      + RES, REMS               +  fast read/write (Byte/AAIP) + SE/BE32/CE/CE_ALT
 * SST 25VF*B      + RES, REMS, RDID         +  fast read/write (Byte/AAIP) + SE/BE32/CE/CE_ALT
 * SST 25VF*C      + RES, REMS, RDID         +  fast/double read/write      + SE/BE32/BE64/CE/CE_ALT
 * SST 25WF*       + RES, REMS, RDID         +  fast read/write (Byte/AAIP) + SE/BE32/CE/CE_ALT
 */

enum FLASHSTATUS {
    WIP  = 0x01,
    WEL  = 0x02,
    BP0  = 0x04,
    BP1  = 0x08,
    BP2  = 0x10,
    SRWD = 0x80
};

enum FLASHFLAGS {
    HASRDID = 0x01,
};

static const char vendors[][16] = {
#define VID_NUMONYX  vendors[0]
    "Numonyx/Micron",
#define VID_MACRONIX vendors[1]
    "Macronix",
#define VID_SST vendors[2]
    "SST/Microchip",
#define VID_ATMEL vendors[3]
    "Atmel/Adesto",
#define VID_ISSI vendors[4]
    "ISSI",
#define VID_WINBOND vendors[5]
    "Winbond",
#define VID_SPANSION vendors[6]
    "Spansion",
#define VID_SANYO vendors[7]
    "Sanyo/ON Semi"
};

typedef struct {
    uint8_t res_id;
    uint8_t vendor_id;
    uint8_t rems_id;
    uint16_t rdid_id;
    const char * vendor;
    const char * device;
    uint8_t addressbytes;
    uint16_t pagesize;
    uint32_t pages;
} flashdata_t;

static const flashdata_t known_memories[] = {
//	 resid  vendor rems  device                                    b/page  pages
    { 0xff, 0x001, 0x15, 0x0215, VID_SPANSION, "S25FL032P",      3,  256, 16384 },
    { 0xff, 0x001, 0x16, 0x0216, VID_SPANSION, "S25FL064P",      3,  256, 32768 },
    { 0xff, 0x001, 0x17, 0x0217, VID_SPANSION, "S25FL128P",      3,  256, 65536 }, // Not in datasheet!
    { 0x17, 0x001, 0x17, 0x0218, VID_SPANSION, "S25FL128S",      3,  256, 65536 }, // assuming not 2018 as in data sheet
    { 0x18, 0x001, 0x18, 0x0219, VID_SPANSION, "S25FL256S",      3,  256, 65536 }, // 2 banks
    { 0x19, 0x001, 0x19, 0x0220, VID_SPANSION, "S25FL512S",      3,  256, 65536 }, // 4 banks
    { 0x17, 0x001, 0x17, 0x2018, VID_SPANSION, "S25FL129P",      3,  256, 65536 },
    { 0x12, 0x001, 0x12, 0x4013, VID_SPANSION, "S25FL204K",      3,  256,  2048 },
    { 0x13, 0x001, 0x13, 0x4014, VID_SPANSION, "S25FL208K",      3,  256,  4096 },
    { 0x14, 0x001, 0x14, 0x4015, VID_SPANSION, "S25FL216K",      3,  256,  8192 },
    { 0xff, 0x01f, 0xff, 0x0400, VID_ATMEL,    "AT26F004",       3,  264,  2048 },
    { 0xff, 0x01f, 0xff, 0x2200, VID_ATMEL,    "AT45DB011D",     3,  264,   512 },
    { 0xff, 0x01f, 0xff, 0x2300, VID_ATMEL,    "AT45DB021D",     3,  264,  1024 },
    { 0xff, 0x01f, 0xff, 0x2400, VID_ATMEL,    "AT45DB041D",     3,  264,  2048 },
    { 0xff, 0x01f, 0xff, 0x2500, VID_ATMEL,    "AT45DB081D",     3,  264,  4096 },
    { 0xff, 0x01f, 0xff, 0x2600, VID_ATMEL,    "AT45DB161D",     3,  528,  4096 },
    { 0xff, 0x01f, 0xff, 0x2700, VID_ATMEL,    "AT45DB321D",     3,  528,  8192 }, // Might be 2701?
    { 0xff, 0x01f, 0xff, 0x2800, VID_ATMEL,    "AT45DB642D",     3, 1056,  8192 },
    { 0xff, 0x01f, 0xff, 0x4400, VID_ATMEL,    "AT26DF041A",     3,  256,  2048 },
    { 0xff, 0x01f, 0xff, 0x4401, VID_ATMEL,    "AT25DF041A",     3,  256,  2048 },
    { 0xff, 0x01f, 0xff, 0x4500, VID_ATMEL,    "AT26DF081",      3,  256,  4096 },
    { 0xff, 0x01f, 0xff, 0x4501, VID_ATMEL,    "AT25/26DF081A",  3,  256,  4096 },
    { 0xff, 0x01f, 0xff, 0x4502, VID_ATMEL,    "AT25DF/DL081",   3,  256,  4096 },
    { 0xff, 0x01f, 0xff, 0x4600, VID_ATMEL,    "AT26DF161",      3,  256,  8192 },
    { 0xff, 0x01f, 0xff, 0x4601, VID_ATMEL,    "AT26DF161A",     3,  256,  8192 },
    { 0xff, 0x01f, 0xff, 0x4602, VID_ATMEL,    "AT25DF161",      3,  256,  8192 },
    { 0xff, 0x01f, 0xff, 0x4700, VID_ATMEL,    "AT25/26DF321",   3,  256, 16384 },
    { 0xff, 0x01f, 0xff, 0x4701, VID_ATMEL,    "AT25DF321A",     3,  256, 16384 },
    { 0xff, 0x01f, 0xff, 0x4800, VID_ATMEL,    "AT25DF641",      3,  256, 32768 },
    { 0xff, 0x01f, 0x60, 0xffff, VID_ATMEL,    "AT25F1024",      3,  256,   512 },
    { 0xff, 0x01f, 0x63, 0xffff, VID_ATMEL,    "AT25F2048",      3,  256,  1024 },
    { 0xff, 0x01f, 0x64, 0xffff, VID_ATMEL,    "AT25F4096",      3,  256,  2048 },
    { 0xff, 0x01f, 0x65, 0x6500, VID_ATMEL,    "AT25F512",       3,  256,   512 },
    { 0xff, 0x01f, 0xff, 0x6601, VID_ATMEL,    "AT25FS010",      3,  256,   512 },
    { 0xff, 0x01f, 0xff, 0x6604, VID_ATMEL,    "AT25FS040",      3,  256,  2048 },
    { 0xff, 0x01f, 0xff, 0x8600, VID_ATMEL,    "AT25DQ161",      3,  256,  8192 },
    { 0xff, 0x01f, 0xff, 0x8700, VID_ATMEL,    "AT25DQ321",      3,  256, 16384 },

    { 0x05, 0x020, 0xff, 0x2010, VID_NUMONYX,  "M25P05",         3,  256,   256 },
    { 0x10, 0x020, 0xff, 0x2011, VID_NUMONYX,  "M25P10",         3,  256,   512 },
    { 0x11, 0x020, 0xff, 0x2012, VID_NUMONYX,  "M25P20",         3,  256,  1024 },
    { 0x12, 0x020, 0xff, 0x2013, VID_NUMONYX,  "M25P40",         3,  256,  2048 },
    { 0x13, 0x020, 0xff, 0x2014, VID_NUMONYX,  "M25P80",         3,  256,  4096 },
    { 0x14, 0x020, 0xff, 0x2015, VID_NUMONYX,  "M25P16",         3,  256,  8192 },
    { 0x15, 0x020, 0xff, 0x2016, VID_NUMONYX,  "M25P32",         3,  256, 16384 },
    { 0x16, 0x020, 0xff, 0x2017, VID_NUMONYX,  "M25P64",         3,  256, 32768 },
    { 0x17, 0x020, 0xff, 0x2018, VID_NUMONYX,  "M25P128",        3,  256, 65536 },
    { 0xff, 0x020, 0xff, 0x4011, VID_NUMONYX,  "M45PE10",        3,  256,   512 },
    { 0xff, 0x020, 0xff, 0x4012, VID_NUMONYX,  "M45PE20",        3,  256,  1024 },
    { 0xff, 0x020, 0xff, 0x4013, VID_NUMONYX,  "M45PE40",        3,  256,  2048 },
    { 0xff, 0x020, 0xff, 0x4014, VID_NUMONYX,  "M45PE80",        3,  256,  4096 },
    { 0xff, 0x020, 0xff, 0x4015, VID_NUMONYX,  "M45PE16",        3,  256,  8192 },
    { 0xff, 0x020, 0xff, 0x7114, VID_NUMONYX,  "M25PX80",        3,  256,  4096 },
    { 0xff, 0x020, 0xff, 0x7115, VID_NUMONYX,  "M25PX16",        3,  256,  8192 },
    { 0xff, 0x020, 0xff, 0x7116, VID_NUMONYX,  "M25PX32",        3,  256, 16384 },
    { 0xff, 0x020, 0xff, 0x7117, VID_NUMONYX,  "M25PX64",        3,  256, 32768 },
    { 0xff, 0x020, 0xff, 0x8011, VID_NUMONYX,  "M25PE10",        3,  256,   512 },
    { 0xff, 0x020, 0xff, 0x8012, VID_NUMONYX,  "M25PE20",        3,  256,  1024 },
    { 0xff, 0x020, 0xff, 0x8013, VID_NUMONYX,  "M25PE40",        3,  256,  2048 },
    { 0xff, 0x020, 0xff, 0x8014, VID_NUMONYX,  "M25PE80",        3,  256,  4096 },
    { 0xff, 0x020, 0xff, 0x8015, VID_NUMONYX,  "M25PE16",        3,  256,  8192 },
    { 0xff, 0x020, 0xff, 0xbb18, VID_NUMONYX,  "N25Q128",        3,  256, 65536 },
    { 0xff, 0x020, 0xff, 0xba19, VID_NUMONYX,  "N25Q256",        3,  256, 65536 }, // 2 banks of 65536 pages each
    { 0xff, 0x020, 0xff, 0xba20, VID_NUMONYX,  "N25Q512",        3,  256, 65536 }, // 4 banks of 65536 pages each
    { 0xff, 0x020, 0xff, 0xba21, VID_NUMONYX,  "N25Q00A",        3,  256, 65536 }, // 8 banks of 65536 pages each

    { 0x44, 0x062, 0xff, 0x0612, VID_SANYO,    "LE25U20",        3,  256,  1024 },
    { 0x6e, 0x062, 0xff, 0x0613, VID_SANYO,    "LE25U40",        3,  256,  1024 },
    { 0x3e, 0x062, 0xff, 0x1613, VID_SANYO,    "LE25S40",        3,  256,  2048 },
    { 0x13, 0x0ef, 0x13, 0x2014, VID_WINBOND,  "W25P80",         3,  256,  4096 },
    { 0x14, 0x0ef, 0x14, 0x2015, VID_WINBOND,  "W25P16",         3,  256,  8192 },
    { 0x15, 0x0ef, 0x15, 0x2016, VID_WINBOND,  "W25P32",         3,  256, 16384 },
    { 0x09, 0x0ef, 0x09, 0x3010, VID_WINBOND,  "W25X05CL",       3,  256,   256 },
    { 0x10, 0x0ef, 0x10, 0x3011, VID_WINBOND,  "W25X10",         3,  256,   512 },
    { 0x11, 0x0ef, 0x11, 0x3012, VID_WINBOND,  "W25X20",         3,  256,  1024 },
    { 0x12, 0x0ef, 0x12, 0x3013, VID_WINBOND,  "W25X40",         3,  256,  2048 },
    { 0x13, 0x0ef, 0x13, 0x3014, VID_WINBOND,  "W25X80",         3,  256,  4096 },
    { 0x14, 0x0ef, 0x14, 0x3015, VID_WINBOND,  "W25X16",         3,  256,  8192 },
    { 0x15, 0x0ef, 0x15, 0x3016, VID_WINBOND,  "W25X32",         3,  256, 16384 },
    { 0x16, 0x0ef, 0x16, 0x3017, VID_WINBOND,  "W25X64",         3,  256, 32768 },
    { 0x12, 0x0ef, 0x12, 0x4013, VID_WINBOND,  "W25Q40BV/CL",    3,  256,  2048 },
    { 0x13, 0x0ef, 0x13, 0x4014, VID_WINBOND,  "W25Q80BV",       3,  256,  4096 },

    { 0x14, 0x0ef, 0x14, 0x4015, VID_WINBOND,  "W25Q16BV/CV/V",  3,  256,  8192 },
    { 0x15, 0x0ef, 0x15, 0x4016, VID_WINBOND,  "W25Q32BV/FV",    3,  256, 16384 }, // FV in SPI mode, 0x6016 in QPI
    { 0x16, 0x0ef, 0x16, 0x4017, VID_WINBOND,  "W25Q64BV/CV/FV", 3,  256, 32768 }, // SPI mode, 0x6017 in QPI
    { 0x17, 0x0ef, 0x17, 0x4018, VID_WINBOND,  "W25Q128BV/FV",   3,  256, 65536 }, // SPI mode, 0x6018 in QPI
    { 0x12, 0x0ef, 0x12, 0x5013, VID_WINBOND,  "W25Q40BW",       3,  256,  2048 },
    { 0x13, 0x0ef, 0x13, 0x5014, VID_WINBOND,  "W25Q80BW",       3,  256,  4096 },
    { 0x14, 0x0ef, 0x14, 0x6015, VID_WINBOND,  "W25Q16DW",       3,  256,  8192 },
    { 0x15, 0x0ef, 0x15, 0x6016, VID_WINBOND,  "W25Q32DW",       3,  256, 16384 },
    { 0x16, 0x0ef, 0x16, 0x6017, VID_WINBOND,  "W25Q64DW",       3,  256, 32768 },
    { 0x32, 0x0ef, 0x32, 0xffff, VID_WINBOND,  "W25B40(A) Bot",  3,  256,  2048 },
    { 0x42, 0x0ef, 0x42, 0xffff, VID_WINBOND,  "W25B40(A) Top",  3,  256,  2048 },
    { 0x10, 0x0ef, 0x10, 0xffff, VID_WINBOND,  "W25P10",         3,  256,   512 },
    { 0x11, 0x0ef, 0x11, 0xffff, VID_WINBOND,  "W25P20",         3,  256,  1024 },
    { 0x12, 0x0ef, 0x12, 0xffff, VID_WINBOND,  "W25P40",         3,  256,  2048 },
    { 0xbf, 0x0bf, 0x48, 0xffff, VID_SST,      "SST25VF512A",    3,  256,   256 },
    { 0xbf, 0x0bf, 0x49, 0xffff, VID_SST,      "SST25VF010A",    3,  256,   512 },
    { 0xbf, 0x0bf, 0x43, 0xffff, VID_SST,      "SST25VF020",     3,  256,  1024 },
    { 0xbf, 0x0bf, 0x8c, 0x258c, VID_SST,      "SST25VF020B",    3,  256,  1024 },
    { 0xbf, 0x0bf, 0x8d, 0x258d, VID_SST,      "SST25VF040B",    3,  256,  2048 },
    { 0xbf, 0x0bf, 0x8e, 0x258e, VID_SST,      "SST25VF080B",    3,  256,  4096 },
    { 0xbf, 0x0bf, 0x41, 0x2541, VID_SST,      "SST25VF016B",    3,  256,  8192 },
    { 0xbf, 0x0bf, 0x4a, 0x254a, VID_SST,      "SST25VF032B",    3,  256, 16394 },
    { 0xbf, 0x0bf, 0x4b, 0x254b, VID_SST,      "SST25VF064C",    3,  256, 32768 },
    { 0xbf, 0x0bf, 0x01, 0x2501, VID_SST,      "SST25WF512",     3,  256,   256 },
    { 0xbf, 0x0bf, 0x02, 0x2502, VID_SST,      "SST25WF010",     3,  256,   512 },
    { 0xbf, 0x0bf, 0x03, 0x2503, VID_SST,      "SST25WF020",     3,  256,  1024 },
    { 0xbf, 0x0bf, 0x04, 0x2504, VID_SST,      "SST25WF040",     3,  256,  2048 },
    { 0x05, 0x0c2, 0x05, 0x2010, VID_MACRONIX, "MX25L512E",      3,  256,   256 },
    { 0x10, 0x0c2, 0x10, 0x2011, VID_MACRONIX, "MX25L1006E",     3,  256,   512 },
    { 0x11, 0x0c2, 0x11, 0x2012, VID_MACRONIX, "MX25L/V2006E",   3,  256,  1024 },
    { 0x12, 0x0c2, 0x12, 0x2013, VID_MACRONIX, "MX25L/V4006E",   3,  256,  2048 },
    { 0x13, 0x0c2, 0x13, 0x2014, VID_MACRONIX, "MX25L/V8006E",   3,  256,  4096 },
    { 0x14, 0x0c2, 0x14, 0x2015, VID_MACRONIX, "MX25L1606E",     3,  256,  8192 },
    { 0x15, 0x0c2, 0x15, 0x2016, VID_MACRONIX, "MX25L3206E",     3,  256, 16384 }, // also MX25L3235E
    { 0x16, 0x0c2, 0x16, 0x2017, VID_MACRONIX, "MX25L6406E",     3,  256, 32768 }, // also MX25L6445E / MX25L6408E / MX25L6473E
    { 0x17, 0x0c2, 0x17, 0x2018, VID_MACRONIX, "MX25L12835E",    3,  256, 65536 }, // also MX25L12845E / MX25L12875F / MX25L12839F
    { 0x18, 0x0c2, 0x18, 0x2019, VID_MACRONIX, "MX25L25635E",    3,  256, 65536 }, // 2 banks, also MX25L25735E
    { 0x24, 0x0c2, 0x24, 0x2415, VID_MACRONIX, "MX25L1633E",     3,  256,  8192 },
    { 0x25, 0x0c2, 0x25, 0x2515, VID_MACRONIX, "MX25L1635E",     3,  256,  8192 },
    { 0x32, 0x0c2, 0x32, 0x2532, VID_MACRONIX, "MX25U2033E",     3,  256,  1024 },
    { 0x33, 0x0c2, 0x33, 0x2533, VID_MACRONIX, "MX25U4035",      3,  256,  2048 }, // also MX25U4033E
    { 0x34, 0x0c2, 0x34, 0x2534, VID_MACRONIX, "MX25U8035",      3,  256,  4096 }, // also MX25U8033E
    { 0x35, 0x0c2, 0x35, 0x2535, VID_MACRONIX, "MX25U1635",      3,  256,  8192 },
    { 0x36, 0x0c2, 0x36, 0x2536, VID_MACRONIX, "MX25U3235",      3,  256, 16384 }, // also MX25U3235F
    { 0x37, 0x0c2, 0x37, 0x2537, VID_MACRONIX, "MX25U6435",      3,  256, 32768 }, // also MX25U6435F
    { 0x38, 0x0c2, 0x38, 0x2538, VID_MACRONIX, "MX25U12835",     3,  256, 65536 },
    { 0x53, 0x0c2, 0x53, 0x2553, VID_MACRONIX, "MX25V4035",      3,  256,  2048 },
    { 0x54, 0x0c2, 0x54, 0x2554, VID_MACRONIX, "MX25V8035",      3,  256,  4096 },
    { 0x87, 0x0c2, 0x87, 0x2617, VID_MACRONIX, "MX25L6455E",     3,  256, 32768 },
    { 0x88, 0x0c2, 0x88, 0x2618, VID_MACRONIX, "MX25L12855E",    3,  256, 65536 },
    { 0x89, 0x0c2, 0x89, 0x2619, VID_MACRONIX, "MX25L25655F",    3,  256, 65536 }, // 4-byte address mode exists...
    { 0x9e, 0x0c2, 0x9e, 0x9e16, VID_MACRONIX, "MX25L3255E",     3,  256, 16384 },

/* ISSI has a high vendor ID with leading 0x7f. Most chips still have two device ID bytes.
   The first is output on RES, the second on RDID (vendor 0x7d, device 0x9d <devid2>.
   On RESM vendor id 1 (0x9d), <devid1> is output.
      did1   0x7f       0x9d did2
             0x9d  did1 */
/*    { 0x05, 0x07f, 0x05, 0x9d20, VID_ISSI,     "IS25CD512",      3,  256,   256 },
    { 0x10, 0x07f, 0x10, 0x9d21, VID_ISSI,     "IS25CD010",      3,  256,   512 },
    { 0x11, 0x07f, 0x11, 0x9d22, VID_ISSI,     "IS25LD020",      3,  256,  1024 },
    { 0x11, 0x07f, 0x11, 0x9d32, VID_ISSI,     "IS25WD020",      3,  256,  1024 },
    { 0x12, 0x07f, 0x12, 0x9d33, VID_ISSI,     "IS25WD040",      3,  256,  2048 },
    { 0x11, 0x07f, 0x11, 0x9d42, VID_ISSI,     "IS25LQ020",      3,  256,  1024 },
    { 0x12, 0x07f, 0x12, 0x9d43, VID_ISSI,     "IS25LQ040",      3,  256,  2048 },
//  { 0x13, 0x07f, 0x13, 0x9d44, VID_ISSI,     "IS25LQ080",      3,  256,  4096 },
//  { 0x14, 0x07f, 0x14, 0x9d45, VID_ISSI,     "IS25LQ016",      3,  256,  8192 },
    { 0x15, 0x07f, 0x15, 0x9d46, VID_ISSI,     "IS25CQ032",      3,  256, 16384 },
    { 0x7e, 0x07f, 0x7e, 0x9d7e, VID_ISSI,     "IS25LD040",      3,  256,  2048 }, // Maybe datasheet error
    */
};

static uint32_t next_free_page = 0;
static uint8_t * buffers[2];
static uint8_t active_buffer = 0;
static uint16_t bufferfill;
static const flashdata_t * memory = (flashdata_t *) 0;

static void write_address (uint32_t address) {
    switch (memory->addressbytes) {
    case 4: cfg->spi_txrx ((address >> 24) & 0xff);
    case 3: cfg->spi_txrx ((address >> 16) & 0xff);
    case 2: cfg->spi_txrx ((address >> 8) & 0xff);
    case 1: cfg->spi_txrx (address & 0xff);
    }
}

static void meminit (void) {
    uint32_t address = 0;
    uint8_t data;

    buffers[0] = malloc (memory->pagesize);
    buffers[1] = malloc (memory->pagesize);
    if (memory->pagesize > 256)
        bufferfill = 2;
    else
        bufferfill = 1;

    for (next_free_page = 0; next_free_page < memory->pages; next_free_page++) {
        cfg->assert();
        cfg->spi_txrx (READ);
        write_address (address);
        data = cfg->spi_txrx (0);
        cfg->deassert();
        if (data == 0xff) // FIXME
            break;
        address += memory->pagesize;
    }
    DEBUGOUT("Flash: %ld/%ld pages used\n", next_free_page, memory->pages);
}

static int flash_id (void) {
    uint8_t resid;
    uint8_t vendorid;
    uint16_t deviceid;
    uint8_t i;

    cfg->assert();
    cfg->spi_txrx(RES);
    cfg->spi_txrx (0);
    cfg->spi_txrx (0);
    cfg->spi_txrx (0);
    resid = cfg->spi_txrx (0);
    cfg->deassert();

    vendorid = 0;
    cfg->assert();
    cfg->spi_txrx (RDID);
    vendorid = cfg->spi_txrx (0);
    deviceid = cfg->spi_txrx (0);
    deviceid <<= 8;
    deviceid |= cfg->spi_txrx (0);
    cfg->deassert();

    DEBUGOUT("RESID: %x, VENDORID: %x, DEVICEID: %x\n", resid, vendorid, deviceid);

    /* Check if RDID yielded something */
    if ((vendorid != 0x00 && deviceid != 0x0000) &&
        (vendorid != 0xff && deviceid != 0xffff)) {
        for (i=0; i<(sizeof (known_memories)/sizeof(known_memories[0])); i++) {
            if (vendorid == known_memories[i].vendor_id &&
                deviceid == known_memories[i].rdid_id) {
                memory = &known_memories[i];
                return 1;
            }
        }
    } else {
        /* else try REMS */
        cfg->assert();
        cfg->spi_txrx (REMS);
        cfg->spi_txrx (0);
        cfg->spi_txrx (0);
        cfg->spi_txrx (0);
        vendorid = cfg->spi_txrx (0);
        deviceid = cfg->spi_txrx (0);
        cfg->deassert();

        if ((vendorid != 0x00 && deviceid != 0x0000) &&
            (vendorid != 0xff && deviceid != 0x00ff)) {
            for (i=0; i<(sizeof (known_memories)/sizeof(known_memories[0])); i++) {
                if (vendorid == known_memories[i].vendor_id &&
                    deviceid == known_memories[i].rems_id) {
                    memory = &known_memories[i];
                    return 1;
                }
            }
        } else {
            /* Where did you get that old Atmel device? */
            cfg->assert();
            cfg->spi_txrx (RDIDA);
            vendorid = cfg->spi_txrx (0);
            deviceid = cfg->spi_txrx (0);
            cfg->deassert();
            if ((vendorid != 0x00 && deviceid != 0x0000) &&
                (vendorid != 0xff && deviceid != 0x00ff)) {
                for (i=0; i<(sizeof (known_memories)/sizeof(known_memories[0])); i++) {
                    if (vendorid == known_memories[i].vendor_id &&
                        deviceid == known_memories[i].rems_id) {
                        memory = &known_memories[i];
                        return 1;
                    }
                }
            } else if (resid != 0x00 && resid != 0xff) {
                /* uh-uh. Let's hope that there's a RES id, and it's unique
                   (for those chips that don't have other id means) */
                for (i=0; i<(sizeof (known_memories)/sizeof(known_memories[0])); i++) {
                    if (resid == known_memories[i].res_id &&
                        known_memories[i].vendor_id == 0xff &&
                        known_memories[i].rems_id == 0xff &&
                        known_memories[i].rdid_id == 0xffff) {
                        memory = &known_memories[i];
                        return 1;
                    }
                }
            }
        }
    }

    if ((vendorid == 0x00 && deviceid == 0x0000) ||
        (vendorid == 0xff && deviceid == 0xffff))
        DEBUGSTR("Flash: No or unidentifiable device installed\n");
    else
        DEBUGOUT("Flash: unknown device (vendor %02x, id %04x)\n", vendorid, deviceid);
    return 0;
}

int flash_init (PRG_CFG_T *c) {

	cfg = c;

    if (!flash_id ()){
    	DEBUGSTR("No memmory detected :(\n");
        return 0;
    }
    DEBUGOUT("Flash: %s %s, %ld pages, %d bytes per page\n",
            memory->vendor, memory->device, memory->pages, memory->pagesize);

    meminit ();

    return 1;
}

void flash_wipe (void) {
    uint8_t status;

    if (!memory)
        return;

    DEBUGSTR ("Wiping Flash...\n");
    cfg->assert();
    cfg->spi_txrx (WREN);
    cfg->deassert();
    do {
        delay_ms (1);
        cfg->assert();
        cfg->spi_txrx (RDSR);
        status = cfg->spi_txrx (0);
        cfg->deassert();
    } while (!(status & WEL));
    cfg->assert();
    cfg->spi_txrx (CE);
    cfg->deassert();
    do {
        delay_ms (1000);
        cfg->assert();
        cfg->spi_txrx (RDSR);
        status = cfg->spi_txrx (0);
        cfg->deassert();
    } while (status & WIP);
    cfg->assert();
    cfg->spi_txrx (WRDI);
    cfg->deassert();
    DEBUGSTR ("Flash wiped.\n");
}

void flash_queue (uint8_t length, const uint8_t * data) {
    uint16_t newlength;

    if (!memory)
        return;

    newlength = bufferfill + length;
    if (newlength > memory->pagesize) {
        flash_flush ();
        active_buffer = !active_buffer;
        if (memory->pagesize > 256)
            bufferfill = 2;
        else
            bufferfill = 1;
    }
    memcpy (&buffers[active_buffer][bufferfill], data, length);
    bufferfill += length;
}

void flash_flush (void) {
    uint32_t address;
    uint8_t status;

    if (!memory)
        return;

    if ((memory->pagesize > 256 && bufferfill == 2) || bufferfill == 1)
        return;

    if (next_free_page >= memory->pages)
        return;

    address = next_free_page * memory->pagesize;
    next_free_page++;
    if (memory->pagesize > 256)
        buffers[active_buffer][1] = bufferfill >> 8;
    buffers[active_buffer][0] = bufferfill & 0xff;

    //TODO COnsider to implement somethig here.
    //while (spi_inuse ());

    do {
        cfg->assert();
        cfg->spi_txrx (RDSR);
        status = cfg->spi_txrx (0);
        cfg->deassert();
    } while (status & WIP);

    cfg->assert();
    cfg->spi_txrx (WREN);
    cfg->deassert();

    do {
        cfg->assert();
        cfg->spi_txrx (RDSR);
        status = cfg->spi_txrx (0);
        cfg->deassert();
    } while (!(status & WEL));

    cfg->assert();
    cfg->spi_txrx (PP);
    write_address (address);
    //spi_bulk_write_read (bufferfill, buffers[active_buffer]);
    //bulk_read_write is basically
    unsigned char length=6;
    char *buffer;

    buffer="yhymka";

    while(length--)
    	cfg->spi_txrx(*(buffer++));    // write byte
    cfg->deassert();
}

void flas_write(const uint8_t * data, uint8_t length){
    uint32_t address = 0;
    uint8_t status;
    uint32_t bytes;

    if (!memory) return;

    do {
        cfg->assert();
        cfg->spi_txrx (RDSR);
        status = cfg->spi_txrx (0);
        cfg->deassert();
    } while (status & WIP);

    cfg->assert();
    cfg->spi_txrx (WREN);
    cfg->deassert();

    do {
        cfg->assert();
        cfg->spi_txrx (RDSR);
        status = cfg->spi_txrx (0);
        cfg->deassert();
    } while (!(status & WEL));

    for (bytes = 0; bytes < length; bytes++) {
		if (bytes == address) {
			cfg->deassert();
		    cfg->assert();
		    cfg->spi_txrx (PP);
			write_address(address);
			address += memory->pagesize;
		}
		cfg->spi_txrx(*(data++));
    }
    cfg->deassert();
}

void flash_dump (void){
    uint8_t  out_buffer[OUT_BUFF_SIZE] = {0};
	uint16_t page;
    uint32_t address = 0;
    uint16_t i,psize;

    if (!memory) return;

	for (page = 0; page < memory->pages; page++){
		cfg->assert();
		cfg->spi_txrx(READ);
		write_address(address);

		for (i=0; i < memory->pagesize/OUT_BUFF_SIZE; i++) {//4
			for (psize=0; psize<OUT_BUFF_SIZE+memory->pagesize%OUT_BUFF_SIZE; psize++) {//64
				out_buffer[psize] = cfg->spi_txrx(0x00);
			}
			vcom_write(&out_buffer, OUT_BUFF_SIZE);
		}
		cfg->deassert();
		address += memory->pagesize;
	}
}

void flash_dump2 (void){
    uint8_t  out_buffer[OUT_BUFF_SIZE] = {0};
    uint32_t address = 0;
    uint32_t total   = memory->pagesize*memory->pages;
    uint32_t bytes   = 0;
    uint8_t  chunk   = 0;

    if (!memory) return;
    cfg->assert();
    for (bytes = 0; bytes < total; bytes++) {
		if (bytes == address) {
			cfg->spi_txrx(READ);
			write_address(address);
			address += memory->pagesize;
		}

		out_buffer[chunk++] = cfg->spi_txrx(0x00);

		if (chunk == OUT_BUFF_SIZE) {
			vcom_write(&out_buffer, chunk);
			chunk = 0;
		}
    }
    if (chunk>0){
    	vcom_write(&out_buffer, chunk);
    }
    cfg->deassert();
}
