#ifndef PROGRAMMER_H_
#define PROGRAMMER_H_

typedef struct prgcfg {
    uint8_t (* spi_txrx)(uint8_t);
    void (* assert)(void);
    void (* deassert)(void);
    void (* reset)(void);
} PRG_CFG_T;

int flash_init (PRG_CFG_T *c);
void flash_flush (void);
void flash_wipe (void);
void flash_dump (void);
void flash_dump2 (void);
void flash_queue (uint8_t length, const uint8_t * data) ;
void flash_flush (void);
void flas_write(const uint8_t * data, uint8_t length);
void flashDataRead(unsigned long address, unsigned int length);

#endif /* PROGRAMMER_H_ */
