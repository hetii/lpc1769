#ifndef DRV_SOFTSPI_H_
#define DRV_SOFTSPI_H_
#include <stdint.h>

void softspi_init(void);
uint8_t softspi_txrx_byte(uint8_t shift);
uint8_t softspi_txrx_buff(uint8_t *block, uint8_t len);

#endif /* DRV_SOFTSPI_H_ */
