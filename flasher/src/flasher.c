#include "board.h"
#include "gpiox.h"
#include "softspi.h"
#include "hardspi.h"
#include "config.h"
#include "programmer.h"
#include "timer.h"
#include "cdc_vcom.h"

extern int xmain(void);

int main(void) {

    SystemCoreClockUpdate();
    Board_Init();
    Board_LED_Set(0, true);
    SysTickInit();

    //softspi_init();
    hardspi_init();
    GPIO_MUX(CDC_USB_GPIO_PULL);
    GPIO_OUT(CDC_USB_GPIO_PULL);
    GPIO_MUX(xFLASH_GPIO_RST);
    GPIO_MUX(xFLASH_GPIO_SEL);
    GPIO_OUT(xFLASH_GPIO_RST);
    GPIO_OUT(xFLASH_GPIO_SEL);
    xmain();
    //GPIO_RST(xFLASH_GPIO_RST);

    PRG_CFG_T c;
    //c.spi_txrx = softspi_txrx_byte;
    c.spi_txrx = hardspi_txrx_byte;

    c.assert = Board_SPI_AssertSSEL;
    c.deassert = Board_SPI_DeassertSSEL;
    uint8_t i;
    flash_init(&c);
    uint8_t b[256]={0};
    while (1){
        i = vcom_bread(&b[0], 256);
        if (i){
			switch (b[0]) {
			case 'e':
				DEBUGSTR("Erase flash!\n");
				flash_wipe();
				break;
			case 'd':
				flash_dump();
				DEBUGSTR("Dump 1 content of the flash!\n");
				break;
			case '2':
				flash_dump2();
				DEBUGSTR("Dump 2 content of the flash!\n");
				break;
			case 'w':
				DEBUGSTR("Write data to flash\n");
				//flas_write("Dupa jasio pierdzi stasio!", 26);
				flas_write("A teraz to!", 11);
				break;
			default:
				break;
		}
}
        __WFI();
    }
    return 0 ;
}
