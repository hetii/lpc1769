#include "stk500isp.h"
#include "board.h"
#include "gpiox.h"
#include "timer.h"
#include "hardspi.h"
#include "softspi.h"
#include "config.h"
#include "softspi.h"


static unsigned char ispClockDelay;
static unsigned char cmdBuffer[4];

//void saf_addEventHandler(void (*callback)(saf_Event)){

/* ------------------------------------------------------------------------- */
/* We disable interrupts while transfer a byte. This ensures that we execute
 * at nominal speed, in spite of aggressive USB polling.
 */
static unsigned char ispBlockTransfer(unsigned char *block, unsigned char len)
{
	return softspi_txrx_buff(block, len);

	unsigned char cnt, shift = 0;//, delay = ispClockDelay;
	NVIC_DisableIRQ(USB_IRQn);
    while(len--){

        cnt = 8;
        shift = *block++;
        do{
            if(shift & 0x80){
            	GPIO_SET(SOFTSPI_GPIO_MOSI);
            } else {
            	GPIO_CLR(SOFTSPI_GPIO_MOSI);
            }
            //delay_us(delay);
            delay_ms(1);
            GPIO_SET(SOFTSPI_GPIO_SCK);

            shift <<= 1;
            if(GPIO_GET(SOFTSPI_GPIO_MISO)){
            //if(GPIO_MISO_IS_SET){
            	shift |= 1;
            }
            //delay_us(delay);
            delay_ms(1);
            GPIO_CLR(SOFTSPI_GPIO_SCK);
        }while(--cnt);
    }
    NVIC_EnableIRQ(USB_IRQn);
    return shift;
}

/* ------------------------------------------------------------------------- */
static void ispPulsePositiveReset(void){

	GPIO_SET(SPI_PROG_RST);
	//Chip_GPIO_WritePortBit(LPC_GPIO, 0, 6, true);//high
	delay_ms(1);
	///Chip_GPIO_WritePortBit(LPC_GPIO, 0, 6, false);//low
	GPIO_CLR(SPI_PROG_RST);
}

static void ispAttachToDevice(unsigned char stk500Delay, unsigned char stabDelay)
{
//	DEBUGSTR("ispAttachToDevice");
    if(stk500Delay == 0){ /* 1.8 MHz nominal */
        ispClockDelay = 0;
    }else if(stk500Delay == 1){ /* 460 kHz nominal */
        ispClockDelay = 0;
    }else if(stk500Delay == 2){ /* 115 kHz nominal */
        ispClockDelay = 1;
    }else if(stk500Delay == 3){ /* 58 kHz nominal */
        ispClockDelay = 2;
    }else{
    	ispClockDelay = (stk500Delay + 1)/4 + (stk500Delay + 1)/16;
    }

    softspi_init();
    //hardspi_init();
    GPIO_MUX(SPI_PROG_RST);
    GPIO_OUT(SPI_PROG_RST);

	delay_ms(stabDelay);
	ispPulsePositiveReset(); // From now reset is low...
	delay_ms(stabDelay);
}

void ispDetachFromDevice(unsigned char removeResetDelay){
	DEBUGSTR("ispDetachFromDevice");
	delay_ms(removeResetDelay);
//	Chip_GPIO_WritePortBit(LPC_GPIO, 0, 6, true);
}

unsigned char   ispEnterProgmode(stkEnterProgIsp_t *param){
unsigned char   i, rval;
	DEBUGSTR("ispEnterProgmode");
    ispAttachToDevice(stkParam.s.sckDuration, param->stabDelay);
    delay_ms(param->cmdExeDelay);
    /* we want for(i = param->synchLoops; i--;), but avrdude sends synchLoops == 0 */
    for(i = 32; i--;){
        rval = ispBlockTransfer(param->cmd, param->pollIndex);
        if(param->pollIndex < 4) ispBlockTransfer(param->cmd + param->pollIndex, 4 - param->pollIndex);
        /* success: we are in sync */
        if(rval == param->pollValue) return STK_STATUS_CMD_OK;

        Chip_GPIO_WritePortBit(LPC_GPIO, 0, 7, true);//SCK high
        delay_ms(ispClockDelay);
        Chip_GPIO_WritePortBit(LPC_GPIO, 0, 7, false);//SCK low
        delay_ms(ispClockDelay);
    }
    ispDetachFromDevice(0);
    return STK_STATUS_CMD_FAILED;   /* failure */
}

void ispLeaveProgmode(stkLeaveProgIsp_t *param){
	DEBUGSTR("ispLeaveProgmode");
    ispDetachFromDevice(param->preDelay);
    delay_ms(param->postDelay);
}


static unsigned char waitUntilReady(unsigned char msTimeout){
	delay_ms(10);
    /*timerSetupTimeout(msTimeout);
    while(deviceIsBusy()){
        if(timerTimeoutOccurred())
            return STK_STATUS_RDY_BSY_TOUT;
    }*/
    return STK_STATUS_CMD_OK;
}

unsigned char ispChipErase(stkChipEraseIsp_t *param){
	DEBUGSTR("ispChipErase");
	unsigned char   maxDelay = param->eraseDelay;
	unsigned char   rval = STK_STATUS_CMD_OK;

    ispBlockTransfer(param->cmd, 4);
    if(param->pollMethod != 0){
        if(maxDelay < 10)   /* allow at least 10 ms */
            maxDelay = 10;
        rval = waitUntilReady(maxDelay);
    }else{
    	delay_ms(maxDelay);
    }
    return rval;
}

unsigned char ispProgramMemory(stkProgramFlashIsp_t *param, unsigned char isEeprom){
	DEBUGSTR("ispProgramMemory");
	utilWord_t  numBytes;
	unsigned char       rval = STK_STATUS_CMD_OK;
	unsigned char       valuePollingMask, rdyPollingMask;
	unsigned int 		i;

    numBytes.bytes[1] = param->numBytes[0];
    numBytes.bytes[0] = param->numBytes[1];
    if(param->mode & 1){    /* page mode */
        valuePollingMask = 0x20;
        rdyPollingMask = 0x40;
    }else{                  /* word mode */
        valuePollingMask = 4;
        rdyPollingMask = 8;
    }
    if(!isEeprom && stkAddress.bytes[3] & 0x80){
        cmdBuffer[0] = 0x4d;    /* load extended address */
        cmdBuffer[1] = 0x00;
        cmdBuffer[2] = stkAddress.bytes[2];
        cmdBuffer[3] = 0x00;
        ispBlockTransfer(cmdBuffer, 4);
    }
    for(i = 0; rval == STK_STATUS_CMD_OK && i < numBytes.word; i++){
        unsigned char x;
        cmdBuffer[1] = stkAddress.bytes[1];
        cmdBuffer[2] = stkAddress.bytes[0];
        cmdBuffer[3] = param->data[i];
        x = param->cmd[0];
        if(!isEeprom){
            x &= ~0x08;
            if((unsigned char)i & 1){
                x |= 0x08;
                stkIncrementAddress();
            }
        }else{
            stkIncrementAddress();
        }
        cmdBuffer[0] = x;
        ispBlockTransfer(cmdBuffer, 4);
        if(param->mode & 1){            /* is page mode */
            if(i < numBytes.word - 1 || !(param->mode & 0x80))
                continue;               /* not last byte written */
            cmdBuffer[0] = param->cmd[1];     /* write program memory page */
            ispBlockTransfer(cmdBuffer, 4);
        }
        /* poll for ready after each byte (word mode) or page (page mode) */
        if(param->mode & valuePollingMask){ /* value polling */
            unsigned char d = param->data[i];
            if(d == param->poll[0] || (isEeprom && d == param->poll[1])){   /* must use timed polling */
            	delay_ms(param->delay);
            }else{
                unsigned char x = param->cmd[2];     /* read memory */
                if(!isEeprom){
                    x &= ~0x08;
                    if((unsigned char)i & 1){
                        x |= 0x08;
                    }
                }
                cmdBuffer[0] = x;
                delay_ms(10);
                //timerSetupTimeout(param->delay);
                while(ispBlockTransfer(cmdBuffer, 4) != d){
                    /*if(timerTimeoutOccurred()){
                        rval = STK_STATUS_CMD_TOUT;
                        break;
                    }*/
                	delay_ms(1);
                }
            }
        }else if(param->mode & rdyPollingMask){ /* rdy/bsy polling */
            rval = waitUntilReady(param->delay);
        }else{                          /* must be timed delay */
        	delay_ms(param->delay);
        }
    }
    return rval;
}

unsigned int ispReadMemory(stkReadFlashIsp_t *param, stkReadFlashIspResult_t *result, unsigned char isEeprom){
	DEBUGSTR("ispReadMemory");
	utilWord_t  numBytes;
	unsigned char       *p, cmd0;
	unsigned int        i;

    cmdBuffer[3] = 0;
    if(!isEeprom && stkAddress.bytes[3] & 0x80){
        cmdBuffer[0] = 0x4d;    /* load extended address */
        cmdBuffer[1] = 0x00;
        cmdBuffer[2] = stkAddress.bytes[2];
        ispBlockTransfer(cmdBuffer, 4);
    }
    numBytes.bytes[1] = param->numBytes[0];
    numBytes.bytes[0] = param->numBytes[1];
    p = result->data;
    result->status1 = STK_STATUS_CMD_OK;
    cmd0 = param->cmd;
    for(i = 0; i < numBytes.word; i++){

        cmdBuffer[1] = stkAddress.bytes[1];
        cmdBuffer[2] = stkAddress.bytes[0];
        if(!isEeprom){
            if((unsigned char)i & 1){
                cmd0 |= 0x08;
                stkIncrementAddress();
            }else{
                cmd0 &= ~0x08;
            }
        }else{
            stkIncrementAddress();
        }
        cmdBuffer[0] = cmd0;
        *p++ = ispBlockTransfer(cmdBuffer, 4);
    }
    *p = STK_STATUS_CMD_OK; /* status2 */
    return numBytes.word + 2;
}

unsigned char ispProgramFuse(stkProgramFuseIsp_t *param){
	DEBUGSTR("ispProgramFuse");
    ispBlockTransfer(param->cmd, 4);
    return STK_STATUS_CMD_OK;
}

unsigned char ispReadFuse(stkReadFuseIsp_t *param){
	DEBUGSTR("ispReadFuse");
	unsigned char   rval;

    rval = ispBlockTransfer(param->cmd, param->retAddr);
    if(param->retAddr < 4)
        ispBlockTransfer(param->cmd + param->retAddr, 4 - param->retAddr);
    return rval;
}

unsigned int ispMulti(stkMultiIsp_t *param, stkMultiIspResult_t *result){
	unsigned char   cnt1, i, *p;

    cnt1 = param->numTx;
    if(cnt1 > param->rxStartAddr)
        cnt1 = param->rxStartAddr;
    ispBlockTransfer(param->txData, cnt1);

    p = result->rxData;
    for(i = 0; i < param->numTx - cnt1; i++){
        unsigned char b = ispBlockTransfer(&param->txData[cnt1] + i, 1);
        if(i < param->numRx)
            *p++ = b;
    }

    for(; i < param->numRx; i++){
        cmdBuffer[0] = 0;
        *p++ = ispBlockTransfer(cmdBuffer, 1);
    }
    *p = result->status1 = STK_STATUS_CMD_OK;
    return (unsigned int)param->numRx + 2;
}
