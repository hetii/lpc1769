#include "board.h"
#include <stdio.h>
#include <string.h>
#include "app_usbd_cfg.h"
#include "cdc_vcom.h"
#include "stopwatch.h"

#include "stk500protocol.h"

static USBD_HANDLE_T g_hUsb;
static uint8_t g_rxBuff[256];

extern const  USBD_HW_API_T hw_api;
extern const  USBD_CORE_API_T core_api;
extern const  USBD_CDC_API_T cdc_api;
/* Since this example only uses CDC class link functions for that clas only */
static const  USBD_API_T g_usbApi = {
	&hw_api,
	&core_api,
	0,
	0,
	0,
	&cdc_api,
	0,
	0x02221101,
};

const  USBD_API_T *g_pUsbApi = &g_usbApi;

/* Initialize pin and clocks for USB0/USB1 port */
static void usb_pin_clk_init(void)
{
	/* enable USB PLL and clocks */
	Chip_USB_Init();
	/* enable USB 1 port on the board */
	Board_USBD_Init(1);
}

/**
 * @brief	Handle interrupt from USB0
 * @return	Nothing
 */
void USB_IRQHandler(void)
{
	USBD_API->hw->ISR(g_hUsb);
}

/* Find the address of interface descriptor for given class type. */
USB_INTERFACE_DESCRIPTOR *find_IntfDesc(const uint8_t *pDesc, uint32_t intfClass)
{
	USB_COMMON_DESCRIPTOR *pD;
	USB_INTERFACE_DESCRIPTOR *pIntfDesc = 0;
	uint32_t next_desc_adr;

	pD = (USB_COMMON_DESCRIPTOR *) pDesc;
	next_desc_adr = (uint32_t) pDesc;

	while (pD->bLength) {
		/* is it interface descriptor */
		if (pD->bDescriptorType == USB_INTERFACE_DESCRIPTOR_TYPE) {

			pIntfDesc = (USB_INTERFACE_DESCRIPTOR *) pD;
			/* did we find the right interface descriptor */
			if (pIntfDesc->bInterfaceClass == intfClass) {
				break;
			}
		}
		pIntfDesc = 0;
		next_desc_adr = (uint32_t) pD + pD->bLength;
		pD = (USB_COMMON_DESCRIPTOR *) next_desc_adr;
	}

	return pIntfDesc;
}

/**
 * @brief	main routine for example
 * @return	Function should not exit.
 */
int main(void)
{
	USBD_API_INIT_PARAM_T usb_param;
	USB_CORE_DESCS_T desc;
	ErrorCode_t ret = LPC_OK;
	uint32_t rdCnt = 0;

	/* Initialize board and chip */
	SystemCoreClockUpdate();
	Board_Init();
    //TODO check taht....
	StopWatch_Init();

	/* enable clocks and pinmux */
	usb_pin_clk_init();
    Chip_IOCON_PinMux(LPC_IOCON, 2, 9,  IOCON_MODE_INACT, IOCON_FUNC0);
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, 2, 9);
    Chip_GPIO_SetPinState(LPC_GPIO, 2, 9, 0);
	/* initialize call back structures */
	memset((void *) &usb_param, 0, sizeof(USBD_API_INIT_PARAM_T));
	usb_param.usb_reg_base = LPC_USB_BASE + 0x200;
	usb_param.max_num_ep = 9;
	usb_param.mem_base = USB_STACK_MEM_BASE;
	usb_param.mem_size = USB_STACK_MEM_SIZE;

	/* Set the USB descriptors */
	desc.device_desc = (uint8_t *) USB_DeviceDescriptor;
	desc.string_desc = (uint8_t *) USB_StringDescriptor;
	/* Note, to pass USBCV test full-speed only devices should have both
	   descriptor arrays point to same location and device_qualifier set to 0.
	 */
	desc.high_speed_desc = (uint8_t *) USB_FsConfigDescriptor;
	desc.full_speed_desc = (uint8_t *) USB_FsConfigDescriptor;
	desc.device_qualifier = 0;

	/* USB Initialization */
	ret = USBD_API->hw->Init(&g_hUsb, &desc, &usb_param);
	if (ret == LPC_OK) {

		/* Init VCOM interface */
		ret = vcom_init(g_hUsb, &desc, &usb_param);
		if (ret == LPC_OK) {
			/*  enable USB interrupts */
			NVIC_EnableIRQ(USB_IRQn);
			/* now connect */
			USBD_API->hw->Connect(g_hUsb, 1);
		}
	}
    //                                                      8     S     T     K     5     0     0     _     2     2
    //char b[] = {0x1B, 0x01, 0x00, 0x0B, 0x0E, 0x01, 0x00, 0x08, 0x53, 0x54, 0x4B, 0x35, 0x30, 0x30, 0x5F, 0x32, 0x02};
	DEBUGSTR("USB CDC class based virtual Comm port example!");

    unsigned int i,c;

	while (1) {
		stkPoll();
		if (vcom_connected() != 0) {
			rdCnt = vcom_bread(&g_rxBuff[0], 256);

			if (rdCnt) {
				//DEBUGOUT("VCOM BREAD: %d: %x", rdCnt, g_rxBuff[0]);
                for(i = 0; i < rdCnt; i++){
                	stkSetRxChar(g_rxBuff[i]);
                }
			}

			//stkEmitTxBytes();

			static unsigned char sendEmptyFrame = 1, buffer[8];
            unsigned char  k = 0;
            while(k < 8 && (c = stkGetTxByte()) >= 0){
                buffer[k++] = c;
            }
            if(k > 0 || sendEmptyFrame){
                sendEmptyFrame = k; //     send an empty block after last data block to indicate transfer end
                vcom_write(buffer, k);
            }
		}

		/* Sleep until next IRQ happens */
		//__WFI();
	}
}
