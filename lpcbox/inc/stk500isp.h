/*
 * isp.h
 *
 *  Created on: 13 lis 2014
 *      Author: hetii
 */

#ifndef ISP_H_
#define ISP_H_

#include "stk500protocol.h"
#include "board.h"

unsigned char ispEnterProgmode(stkEnterProgIsp_t *param);
void          ispLeaveProgmode(stkLeaveProgIsp_t *param);
unsigned char ispChipErase(stkChipEraseIsp_t *param);
unsigned char ispProgramMemory(stkProgramFlashIsp_t *param, unsigned char isEeprom);
unsigned int  ispReadMemory(stkReadFlashIsp_t *param, stkReadFlashIspResult_t *result, unsigned char isEeprom);
unsigned char ispProgramFuse(stkProgramFuseIsp_t *param);
unsigned char ispReadFuse(stkReadFuseIsp_t *param);
unsigned int  ispMulti(stkMultiIsp_t *param, stkMultiIspResult_t *result);

#endif /* ISP_H_ */
