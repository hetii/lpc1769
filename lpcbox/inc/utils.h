/*
 * util.h
 *
 *  Created on: 13 lis 2014
 *      Author: hetii
 */

#ifndef UTILS_H_
#define UTILS_H_

typedef union{
    unsigned int    word;
    unsigned char   bytes[2];
}utilWord_t;

typedef union{
    unsigned long   dword;
    unsigned char   bytes[4];
}utilDword_t;

/* ------------------------------------------------------------------------- */

static inline unsigned char utilHi8(unsigned int x)
{
utilWord_t converter;

    converter.word = x;
    return converter.bytes[1];
}

#endif /* UTILS_H_ */
