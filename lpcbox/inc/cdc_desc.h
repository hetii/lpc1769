/*
 * cdc_desc.h
 *
 *  Created on: 6 lis 2014
 *      Author: hetii
 */

#ifndef CDC_DESC_H_
#define CDC_DESC_H_

#include "usbd.h"

#define CDC_DESC_INTERFACE(bInterfaceNumber, bDataInterface) \
	USB_INTERFACE_DESC_SIZE,			/* bLength */\
	USB_INTERFACE_DESCRIPTOR_TYPE,		/* bDescriptorType */\
	bInterfaceNumber,					/* bInterfaceNumber: Number of Interface */\
	0x00,								/* bAlternateSetting: Alternate setting */\
	0x01,								/* bNumEndpoints: One endpoint used */\
	CDC_COMMUNICATION_INTERFACE_CLASS,	/* bInterfaceClass: Communication Interface Class */\
	CDC_ABSTRACT_CONTROL_MODEL,			/* bInterfaceSubClass: Abstract Control Model */\
	0x00,								/* bInterfaceProtocol: no protocol used */\
	0x04,								/* iInterface: */\
	/* Header Functional Descriptor*/\
	0x05,								/* bLength: CDC header Descriptor size */\
	CDC_CS_INTERFACE,					/* bDescriptorType: CS_INTERFACE */\
	CDC_HEADER,							/* bDescriptorSubtype: Header Func Desc */\
	WBVAL(CDC_V1_10),					/* bcdCDC 1.10 */\
	/* Call Management Functional Descriptor*/\
	0x05,								/* bFunctionLength */\
	CDC_CS_INTERFACE,					/* bDescriptorType: CS_INTERFACE */\
	CDC_CALL_MANAGEMENT,				/* bDescriptorSubtype: Call Management Func Desc */\
	0x01,								/* bmCapabilities: device handles call management */\
	bDataInterface,						/* bDataInterface: CDC data IF ID */\
	/* Abstract Control Management Functional Descriptor*/\
	0x04,								/* bFunctionLength */\
	CDC_CS_INTERFACE,					/* bDescriptorType: CS_INTERFACE */\
	CDC_ABSTRACT_CONTROL_MANAGEMENT,	/* bDescriptorSubtype: Abstract Control Management desc */\
	0x02,								/* bmCapabilities: SET_LINE_CODING, GET_LINE_CODING, SET_CONTROL_LINE_STATE supported */\
	/* Union Functional Descriptor*/\
	0x05,								/* bFunctionLength */\
	CDC_CS_INTERFACE,					/* bDescriptorType: CS_INTERFACE */\
	CDC_UNION,							/* bDescriptorSubtype: Union func desc */\
	bInterfaceNumber,					/* bMasterInterface: Communication class interface is master */\
	bDataInterface,						/* bSlaveInterface0: Data class interface is slave 0 */\
	/* Endpoint 1 Descriptor*/\
	USB_ENDPOINT_DESC_SIZE,				/* bLength */\
	USB_ENDPOINT_DESCRIPTOR_TYPE,		/* bDescriptorType */\
	USB_CDC_INT_EP,						/* bEndpointAddress */\
	USB_ENDPOINT_TYPE_INTERRUPT,		/* bmAttributes */\
	WBVAL(0x0010),						/* wMaxPacketSize */\
	0x02,			/* 2ms */           /* bInterval */\
	/* Interface 2, Alternate Setting 0, Data class interface descriptor*/\
	USB_INTERFACE_DESC_SIZE,			/* bLength */\
	USB_INTERFACE_DESCRIPTOR_TYPE,		/* bDescriptorType */\
	bDataInterface,						/* bInterfaceNumber: Number of Interface */\
	0x00,								/* bAlternateSetting: no alternate setting */\
	0x02,								/* bNumEndpoints: two endpoints used */\
	CDC_DATA_INTERFACE_CLASS,			/* bInterfaceClass: Data Interface Class */\
	0x00,								/* bInterfaceSubClass: no subclass available */\
	0x00,								/* bInterfaceProtocol: no protocol used */\
	0x04,								/* iInterface: */\
	/* Endpoint, EP Bulk Out */\
	USB_ENDPOINT_DESC_SIZE,				/* bLength */\
	USB_ENDPOINT_DESCRIPTOR_TYPE,		/* bDescriptorType */\
	USB_CDC_OUT_EP,						/* bEndpointAddress */\
	USB_ENDPOINT_TYPE_BULK,				/* bmAttributes */\
	WBVAL(USB_FS_MAX_BULK_PACKET),		/* wMaxPacketSize */\
	0x00,								/* bInterval: ignore for Bulk transfer */\
	/* Endpoint, EP Bulk In */\
	USB_ENDPOINT_DESC_SIZE,				/* bLength */\
	USB_ENDPOINT_DESCRIPTOR_TYPE,		/* bDescriptorType */\
	USB_CDC_IN_EP,						/* bEndpointAddress */\
	USB_ENDPOINT_TYPE_BULK,				/* bmAttributes */\
	WBVAL(64),							/* wMaxPacketSize */\
	0x00\


#endif /* CDC_DESC_H_ */
