#include "board.h"
#include "cdc_vcom.h"
#include "timer.h"
#include <cr_section_macros.h>

extern int stkPool(void);

int main(void) {
    SystemCoreClockUpdate();
    Board_Init();
    SysTickInit();
    stkPool();

    /*    while(1) {
    	vcom_write("hELLO FROM HELl\n", 16);
    	delay_ms(2000);
    }*/
    return 0 ;
}
