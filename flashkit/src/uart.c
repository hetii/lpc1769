#include "board.h"
#include "uart.h"
#include "cdc_vcom.h"

void uart_init(void) 
{
        VcomInit();
}
static unsigned int cipsko=0;
/* send one character to the rs232 */
int uart_sendchar(char c)
{

	vcom_write(&c, 1);
	return 0;
	kurwa[cipsko++] = c;
	if (cipsko > 8){
		vcom_write(kurwa, 8);
		cipsko = 0;
	}
	return cipsko;
	/*
	char d[1];
	d[0] = c;
	vcom_write(d, 1);*/
}
/* send string to the rs232 */
void uart_sendstr(char *s) 
{
        while (*s){
                uart_sendchar(*s);
                s++;
        }
}

/* get a byte from rs232
* this function does a blocking read */
unsigned char uart_getchar(unsigned char kickwd)  
{
	    uint32_t rdCnt=0;
	    static uint8_t g_rxBuff[1];

        while(1){
        	 rdCnt = vcom_bread(&g_rxBuff[0], 1);

             if (rdCnt > 0){

            	 DEBUGOUT("Get data: %d -> %x \n", rdCnt, g_rxBuff[0]);
            	 return g_rxBuff[0];
             }
        }
        return 0x00;
}
