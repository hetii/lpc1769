#include "timer.h"
// timing for software spi:
#define F_CPU 3686400UL  // 3.6864 MHz

static unsigned char sck_dur=1;
static unsigned char spi_in_sw=0;
//static unsigned char sck_dur=12;
//static unsigned char spi_in_sw=1;

// set the speed (used by PARAM_SCK_DURATION, page 31 in AVR068)
// For a clock of 3.6864MHz this is:
// SPI2X SPR1 SPR0 divider result_spi_Freq   avrisp dur
//  0     0    0     f/4      921KHz :        sckdur =0
//  0     0    1     f/16     230KHz :        sckdur =1
//  0     1    0     f/64     57KHz  :        sckdur =2
//  0     1    1     f/128    28KHz  :        sckdur =3
//
//SPI2X=SPSR bit 0
//SPR0 and SPR1 =SPCR bit 0 and 1
unsigned char  spi_set_sck_duration(unsigned char dur)
{
        /*spi_in_sw=0;
        if (dur >= 4){
        	// sofware spi very slow
                spi_in_sw=1;
                sck_dur=12;
        	return(sck_dur);
        }
        if (dur >= 3){
        	// 28KHz
        	cbi(SPSR,SPI2X);
        	sbi(SPCR,SPR1);
        	sbi(SPCR,SPR0);
                sck_dur=3;
        	return(sck_dur);
        }
        if (dur >= 2){
        	// 57KHz
        	cbi(SPSR,SPI2X);
        	sbi(SPCR,SPR1);
        	cbi(SPCR,SPR0);
                sck_dur=2;
        	return(sck_dur);
        }
        if (dur == 1){
        	// 230KHz
        	cbi(SPSR,SPI2X);
        	cbi(SPCR,SPR1);
        	sbi(SPCR,SPR0);
                sck_dur=1;
        	return(sck_dur);
        }
        if (dur == 0){
                // 921KHz
                cbi(SPSR,SPI2X);
                cbi(SPCR,SPR0);
                cbi(SPCR,SPR1);
                sck_dur=0;
                return(sck_dur);
        }*/
        return(1); // we should never come here
}

unsigned char spi_get_sck_duration(void)
{
        return(sck_dur);
}

void spi_sck_pulse(void)
{
	Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 15); // SCK low
	_delay_ms(0.10);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 15); // SCK high
	_delay_ms(0.10);
	Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 15); // SCK low

}

void spi_reset_pulse(void) 
{
        /* give a positive RESET pulse, we can't guarantee
         * that SCK was low during power up (see Atmel's
         * data sheets, search for "Serial Downloading in e.g atmega8 
         * data sheet):
         * the programmer can not guarantee that SCK is held low during
         * Power-up. In this case, RESET must be given a positive pulse of at least two
         * CPU clock cycles duration after SCK has been set to 0."*/
        //PORTB|= (1<<PB2); // reset = high = not active
		Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 16);  /* give positive RESET pulse */
        _delay_ms(0.10);
        Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 16);  /* give positive RESET pulse */
       // PORTB &= ~(1<<PB2); // reset = low, stay active
        delay_ms(20); // min stab delay
}

void spi_init(void) 
{
	hardspi_init();
	Chip_SPI_SetClockCounter(LPC_SPI, 254);
	//Chip_SPI_SetClockCounter(LPC_SPI, 8);
    Chip_IOCON_PinMux(LPC_IOCON, 0, 16,  IOCON_MODE_PULLUP, IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 16); //RST


    /*Chip_IOCON_PinMux(LPC_IOCON, 0, 15,  IOCON_MODE_PULLUP, IOCON_FUNC0);
    Chip_IOCON_PinMux(LPC_IOCON, 0, 17,  IOCON_MODE_INACT, IOCON_FUNC0);
    Chip_IOCON_PinMux(LPC_IOCON, 0, 18,  IOCON_MODE_PULLUP, IOCON_FUNC0);
    Chip_GPIO_SetPinDIRInput (LPC_GPIO, 0, 17); //Miso
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 18); //Mosi
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 15); //SCK*/

	Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 16); // reset = high = not active
	//Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 18);  // MOSI low
	//Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 15);  // SCK low
	delay_ms(20); // discharge MOSI/SCK
	Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 16); //Reset low
    delay_ms(20); // stab delay
    spi_reset_pulse();

}
uint8_t spi_mastertransmit(uint8_t shift){
	Board_LED_Toggle(0);
	return hardspi_txrx_byte(shift);
}

uint8_t Bspi_mastertransmit(uint8_t shift){
    uint8_t cnt = 8;
    do{
        if(shift & 0x80){
        	Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 18);
            //GPIO_SET(SOFTSPI_GPIO_MOSI);
        } else {
        	Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 18);
            //GPIO_CLR(SOFTSPI_GPIO_MOSI);
        }
        _delay_ms(1);
        Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 15);
        //GPIO_SET(SOFTSPI_GPIO_SCK);
        shift <<= 1;
        if(Chip_GPIO_GetPinState(LPC_GPIO,0,17)){
            shift |= 1;
        }
        _delay_ms(1);
        Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 15);
        //GPIO_CLR(SOFTSPI_GPIO_SCK);
    } while(--cnt);
    return shift;
}

// send 8 bit, return received byte
unsigned char Pspi_mastertransmit(unsigned char data)
{
        unsigned char i=128;
        unsigned char rval=0;

        // software spi, slow
       // cbi(PORTB,PB5); // SCK low
        Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 15);
        while(i){
                // MOSI 
                if (data&i){
                	Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 18);

                }else{
                	Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 18);
                }
                _delay_ms(0.10);
                // read MISO
                if(Chip_GPIO_GetPinState(LPC_GPIO,0,17)){
                        rval|= i;
                }
                Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 15);
                //sbi(PORTB,PB5); // SCK high
                _delay_ms(0.10);
                //cbi(PORTB,PB5); // SCK low
                Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 15);
                i=i>>1;
        }
        return(rval);
}

// send 16 bit, return last rec byte
unsigned char spi_mastertransmit_16(unsigned int data)
{
        spi_mastertransmit((data>>8)&0xFF);
        return(spi_mastertransmit(data&0xFF));
}

// send 32 bit, return last rec byte
unsigned char spi_mastertransmit_32(unsigned long data)
{
        spi_mastertransmit((data>>24)&0xFF);
        spi_mastertransmit((data>>16)&0xFF);
        spi_mastertransmit((data>>8)&0xFF);
        return(spi_mastertransmit(data&0xFF));
}


void spi_disable(void)
{
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 16); // reset = high = not active
	/*Chip_GPIO_SetPinDIRInput (LPC_GPIO, 0, 15);
	Chip_GPIO_SetPinDIRInput (LPC_GPIO, 0, 16);
	Chip_GPIO_SetPinDIRInput (LPC_GPIO, 0, 17);
	Chip_GPIO_SetPinDIRInput (LPC_GPIO, 0, 18);

    Chip_IOCON_PinMux(LPC_IOCON, 0, 15,  IOCON_MODE_INACT, IOCON_FUNC0);
    Chip_IOCON_PinMux(LPC_IOCON, 0, 16,  IOCON_MODE_INACT, IOCON_FUNC0);
    Chip_IOCON_PinMux(LPC_IOCON, 0, 17,  IOCON_MODE_INACT, IOCON_FUNC0);
    Chip_IOCON_PinMux(LPC_IOCON, 0, 18,  IOCON_MODE_INACT, IOCON_FUNC0);*/
}

