/* vim: set sw=8 ts=8 si et: */
/*************************************************************************
 Title:   C include file for uart
 Target:    atmega8
 Copyright: GPL
***************************************************************************/
#ifndef asdasUART_H
#define asdasUART_H

extern void uart_init(void);
extern int uart_sendchar(char c);
extern void uart_sendstr(char *s);
//extern void uart_sendstr_p(const prog_char *progmem_s);
extern unsigned char uart_getchar(unsigned char kickwd);
extern void uart_flushRXbuf(void);

unsigned char kurwa[64];


#endif /* UART_H */
