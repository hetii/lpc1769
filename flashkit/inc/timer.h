#ifndef MIFARE_DRV_TIMER_H_
#define MIFARE_DRV_TIMER_H_
#include "board.h"

volatile uint32_t msTicks; // counter for 1ms SysTicks
volatile unsigned char  timerTimeoutCnt;
volatile unsigned char  timerLongTimeoutCnt;

// ms_delay - creates a delay of the appropriate number of Systicks (happens every 1 ms)
__INLINE static void delay_ms (uint32_t delayTicks) {
  uint32_t currentTicks;

  currentTicks = msTicks;   // read current tick counter
  // Now loop until required number of ticks passes.
  while ((msTicks - currentTicks) < delayTicks)
  {
      __WFI();
  }
}
__INLINE static void _delay_ms (uint32_t delayTicks) {
	uint16_t t=25;
	while (t--){
		//__WFI();
	}
}


extern void SysTickInit(void);


typedef struct
{
  void (*delayUs)(uint32_t);
  void (*delayMs)(uint32_t);
  bool (*enabled)(void);
  void (*startMs)(uint32_t);
  void (*stop)(void);
  bool (*expired)(void);
} timerx_t;

extern const timerx_t g_timer32b0m0;
extern const timerx_t g_timer32b0m1;

#endif /* MIFARE_DRV_TIMER_H_ */
