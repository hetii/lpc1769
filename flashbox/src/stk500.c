#include <string.h>
#include "stk500.h"
#include "config.h"
#include "timer.h"
#include "gpiox.h"
#include "softspi.h"
#include "hardspi.h"

/* The following versions are reported to the programming software: */
#define STK_VERSION_HW      1
#define STK_VERSION_MAJOR   2
#define STK_VERSION_MINOR   4
#define BUFFER_SIZE         281 /* results in 275 bytes max body size */
#define RX_TIMEOUT          200 /* timeout in milliseconds */
#define STK_TXMSG_START     5

static unsigned char rxBuffer[BUFFER_SIZE];
static unsigned char txBuffer[BUFFER_SIZE];
static unsigned char cmdBuffer[4];
static unsigned char rxBlockAvailable;
static unsigned char ispClockDelay;
static uint8_t  rxPos=0, txPos, txLen;

static utilWord_t    rxLen;

stkParam_t stkParam = {{
                    0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0,
                    STK_VERSION_HW, STK_VERSION_MAJOR, STK_VERSION_MINOR, 0, 50, 0, 1, 0x80,
                    2, 0, 0xaa, 0, 0, 0, 0, 0,
                }};
utilDword_t stkAddress;


void timerSetupLongTimeout(unsigned char t){
	timerLongTimeoutCnt = t;
}

void timerSetupTimeout(unsigned char t){
	timerTimeoutCnt = t++;
}

unsigned char timerLongTimeoutOccurred(){
	return timerLongTimeoutCnt == 0;
}

unsigned char  timerTimeoutOccurred(void){
	return timerTimeoutCnt == 0;
}

void stkIncrementAddress(void) {
    stkAddress.dword++;
}

static void stkSetTxMessage(uint8_t len) {
	unsigned char   *p = txBuffer, sum = 0;

    *p++ = STK_STX;
    *p++ = rxBuffer[1];  /* sequence number */
    *p++ = utilHi8(len);
    *p++ = len;
    *p++ = STK_TOKEN;
    txPos = 0;
    len += 6;
    txLen = len--;
    p = txBuffer;
    while(len--){
        sum ^= *p++;
    }
    *p = sum;
}

//TODO fix it.
static void setParameter(unsigned char index, unsigned char value) {

    if(index == STK_PARAM_OSC_PSCALE){
        //HW_SET_T2_PRESCALER(value);
    }else if(index == STK_PARAM_OSC_CMATCH){
        //OCR2 = value;
    }
    index &= 0x1f;
    stkParam.bytes[index] = value;
}

//TODO the same as above.
static unsigned char getParameter(unsigned char index) {

    if(index == STK_PARAM_OSC_PSCALE)
        //return HW_GET_T2_PRESCALER();
    	return 1;
    if(index == STK_PARAM_OSC_CMATCH)
        //return OCR2;
    	return 1;
    index &= 0x1f;
    return stkParam.bytes[index];
}

#define SWITCH_START        switch(cmd){{
#define SWITCH_CASE(value)  }break; case (value):{
#define SWITCH_CASE2(v1,v2) }break; case (v1): case(v2):{
#define SWITCH_CASE3(v1,v2,v3) }break; case (v1): case(v2): case(v3):{
#define SWITCH_CASE4(v1,v2,v3,v4) }break; case (v1): case(v2): case(v3): case(v4):{
#define SWITCH_DEFAULT      }break; default:{
#define SWITCH_END          }}

// Not static to prevent inlining.
void stkEvaluateRxMessage(void) {
	unsigned char       i, cmd;
	utilWord_t  len = {2};  /* defaults to cmd + error code */
	void        *param;

    cmd = rxBuffer[STK_TXMSG_START];
    txBuffer[STK_TXMSG_START] = cmd;
    txBuffer[STK_TXMSG_START + 1] = STK_STATUS_CMD_OK;
    param = &rxBuffer[STK_TXMSG_START + 1];
    SWITCH_START
    SWITCH_CASE(STK_CMD_SIGN_ON)
    	//DEBUGSTR("STK_CMD_SIGN_ON\n");
        static const char string[] = {8, 'S', 'T', 'K', '5', '0', '0', '_', '2', 0};
        char *p = (char *)&txBuffer[STK_TXMSG_START + 2];
        strcpy(p, string);
        len.bytes[0] = 11;
    SWITCH_CASE(STK_CMD_SET_PARAMETER)
        //DEBUGSTR("STK_CMD_SET_PARAMETER\n");
        setParameter(rxBuffer[STK_TXMSG_START + 1], rxBuffer[STK_TXMSG_START + 2]);
    SWITCH_CASE(STK_CMD_GET_PARAMETER)
    	//DEBUGSTR("STK_CMD_GET_PARAMETER\n");
        txBuffer[STK_TXMSG_START + 2] = getParameter(rxBuffer[STK_TXMSG_START + 1]);
        len.bytes[0] = 3;
    SWITCH_CASE(STK_CMD_OSCCAL)
        //DEBUGSTR("STK_CMD_OSCCAL\n");
        txBuffer[STK_TXMSG_START + 1] = STK_STATUS_CMD_FAILED;
        /* not yet implemented */
    SWITCH_CASE(STK_CMD_LOAD_ADDRESS)
    	//DEBUGSTR("STK_CMD_LOAD_ADDRESS\n");
        for(i=0;i<4;i++){
            stkAddress.bytes[3-i] = rxBuffer[STK_TXMSG_START + 1 + i];
        }
    SWITCH_CASE(STK_CMD_SET_CONTROL_STACK)
        /* AVR Studio sends:
        1b 08 00 21 0e 2d
        4c 0c 1c 2c 3c 64 74 66
        68 78 68 68 7a 6a 68 78
        78 7d 6d 0c 80 40 20 10
        11 08 04 02 03 08 04 00
        bf
        */
        /* dummy: ignore */
#if ENABLE_HVPROG
    SWITCH_CASE(STK_CMD_ENTER_PROGMODE_HVSP)
        hvspEnterProgmode(param);
    SWITCH_CASE(STK_CMD_LEAVE_PROGMODE_HVSP)
        hvspLeaveProgmode(param);
    SWITCH_CASE(STK_CMD_CHIP_ERASE_HVSP)
        txBuffer[STK_TXMSG_START + 1] = hvspChipErase(param);
    SWITCH_CASE(STK_CMD_PROGRAM_FLASH_HVSP)
        txBuffer[STK_TXMSG_START + 1] = hvspProgramMemory(param, 0);
    SWITCH_CASE(STK_CMD_READ_FLASH_HVSP)
        len.word = 1 + hvspReadMemory(param, (void *)&txBuffer[STK_TXMSG_START + 1], 0);
    SWITCH_CASE(STK_CMD_PROGRAM_EEPROM_HVSP)
        txBuffer[STK_TXMSG_START + 1] = hvspProgramMemory(param, 1);
    SWITCH_CASE(STK_CMD_READ_EEPROM_HVSP)
        len.word = 1 + hvspReadMemory(param, (void *)&txBuffer[STK_TXMSG_START + 1], 1);
    SWITCH_CASE(STK_CMD_PROGRAM_FUSE_HVSP)
        txBuffer[STK_TXMSG_START + 1] = hvspProgramFuse(param);
    SWITCH_CASE(STK_CMD_READ_FUSE_HVSP)
        txBuffer[STK_TXMSG_START + 2] = hvspReadFuse(param);
        len.bytes[0] = 3;
    SWITCH_CASE(STK_CMD_PROGRAM_LOCK_HVSP)
        txBuffer[STK_TXMSG_START + 1] = hvspProgramLock(param);
    SWITCH_CASE(STK_CMD_READ_LOCK_HVSP)
        txBuffer[STK_TXMSG_START + 2] = hvspReadLock();
        len.bytes[0] = 3;
    SWITCH_CASE(STK_CMD_READ_SIGNATURE_HVSP)
        txBuffer[STK_TXMSG_START + 2] = hvspReadSignature(param);
        len.bytes[0] = 3;
    SWITCH_CASE(STK_CMD_READ_OSCCAL_HVSP)
        txBuffer[STK_TXMSG_START + 2] = hvspReadOsccal();
        len.bytes[0] = 3;

    SWITCH_CASE(STK_CMD_ENTER_PROGMODE_PP)
        ppEnterProgmode(param);
    SWITCH_CASE(STK_CMD_LEAVE_PROGMODE_PP)
        ppLeaveProgmode(param);
    SWITCH_CASE(STK_CMD_CHIP_ERASE_PP)
        txBuffer[STK_TXMSG_START + 1] = ppChipErase(param);
    SWITCH_CASE(STK_CMD_PROGRAM_FLASH_PP)
        txBuffer[STK_TXMSG_START + 1] = ppProgramMemory(param, 0);
    SWITCH_CASE(STK_CMD_READ_FLASH_PP)
        len.word = 1 + ppReadMemory(param, (void *)&txBuffer[STK_TXMSG_START + 1], 0);
    SWITCH_CASE(STK_CMD_PROGRAM_EEPROM_PP)
        txBuffer[STK_TXMSG_START + 1] = ppProgramMemory(param, 1);
    SWITCH_CASE(STK_CMD_READ_EEPROM_PP)
        len.word = 1 + ppReadMemory(param, (void *)&txBuffer[STK_TXMSG_START + 1], 1);
    SWITCH_CASE(STK_CMD_PROGRAM_FUSE_PP)
        txBuffer[STK_TXMSG_START + 1] = ppProgramFuse(param);
    SWITCH_CASE(STK_CMD_READ_FUSE_PP)
        txBuffer[STK_TXMSG_START + 2] = ppReadFuse(param);
        len.bytes[0] = 3;
    SWITCH_CASE(STK_CMD_PROGRAM_LOCK_PP)
        txBuffer[STK_TXMSG_START + 1] = ppProgramLock(param);
    SWITCH_CASE(STK_CMD_READ_LOCK_PP)
        txBuffer[STK_TXMSG_START + 2] = ppReadLock();
        len.bytes[0] = 3;
    SWITCH_CASE(STK_CMD_READ_SIGNATURE_PP)
        txBuffer[STK_TXMSG_START + 2] = ppReadSignature(param);
        len.bytes[0] = 3;
    SWITCH_CASE(STK_CMD_READ_OSCCAL_PP)
        txBuffer[STK_TXMSG_START + 2] = ppReadOsccal();
        len.bytes[0] = 3;
#endif /* ENABLE_HVPROG */
    SWITCH_CASE(STK_CMD_ENTER_PROGMODE_ISP)
        //DEBUGSTR("STK_CMD_ENTER_PROGMODE_ISP\n");
        txBuffer[STK_TXMSG_START + 1] = ispEnterProgmode(param);
    SWITCH_CASE(STK_CMD_LEAVE_PROGMODE_ISP)
        //DEBUGSTR("STK_CMD_LEAVE_PROGMODE_ISP\n");
        ispLeaveProgmode(param);
    SWITCH_CASE(STK_CMD_CHIP_ERASE_ISP)
        DEBUGSTR("STK_CMD_CHIP_ERASE_ISP\n");
        txBuffer[STK_TXMSG_START + 1] = ispChipErase(param);
    SWITCH_CASE(STK_CMD_PROGRAM_FLASH_ISP)
        //DEBUGSTR("STK_CMD_PROGRAM_FLASH_ISP\n");
        txBuffer[STK_TXMSG_START + 1] = ispProgramMemory(param, 0);
    SWITCH_CASE(STK_CMD_READ_FLASH_ISP)
        //DEBUGSTR("STK_CMD_READ_FLASH_ISP\n");
        len.word = 1 + ispReadMemory(param, (void *)&txBuffer[STK_TXMSG_START + 1], 0);
    SWITCH_CASE(STK_CMD_PROGRAM_EEPROM_ISP)
        //DEBUGSTR("STK_CMD_PROGRAM_EEPROM_ISP\n");
        txBuffer[STK_TXMSG_START + 1] = ispProgramMemory(param, 1);
    SWITCH_CASE(STK_CMD_READ_EEPROM_ISP)
        //DEBUGSTR("STK_CMD_READ_EEPROM_ISP\n");
        len.word = 1 + ispReadMemory(param, (void *)&txBuffer[STK_TXMSG_START + 1], 1);
    SWITCH_CASE(STK_CMD_PROGRAM_FUSE_ISP)
        //DEBUGSTR("STK_CMD_PROGRAM_FUSE_ISP\n");
        txBuffer[STK_TXMSG_START + 1] = ispProgramFuse(param);
    SWITCH_CASE4(STK_CMD_READ_FUSE_ISP, STK_CMD_READ_LOCK_ISP, STK_CMD_READ_SIGNATURE_ISP, STK_CMD_READ_OSCCAL_ISP)
        //DEBUGSTR("STK_CMD_READ_FUSE_ISP, STK_CMD_READ_LOCK_ISP, STK_CMD_READ_SIGNATURE_ISP, STK_CMD_READ_OSCCAL_ISP\n");
        txBuffer[STK_TXMSG_START + 2] = ispReadFuse(param);
        txBuffer[STK_TXMSG_START + 3] = STK_STATUS_CMD_OK;
        len.bytes[0] = 4;
    SWITCH_CASE(STK_CMD_PROGRAM_LOCK_ISP)
        //DEBUGSTR("STK_CMD_PROGRAM_LOCK_ISP\n");
        txBuffer[STK_TXMSG_START + 1] = ispProgramFuse(param);
    SWITCH_CASE(STK_CMD_SPI_MULTI)
        //DEBUGSTR("STK_CMD_SPI_MULTI\n");
        len.word = 1 + ispMulti(param, (void *)&txBuffer[STK_TXMSG_START + 1]);
    SWITCH_DEFAULT  /* unknown command */
		DEBUGSTR("unknown command  END\n");
        txBuffer[STK_TXMSG_START + 1] = STK_STATUS_CMD_FAILED;
    SWITCH_END
    stkSetTxMessage(len.word);
   // DEBUGSTR("stkEvaluateRxMessage END\n");
}


/* This is alternative for stkGetTxBytem
 *  where we send what we have. */
/*void stkEmitTxBytes(void){
    if(txLen > 0){
    	vcom_write(txBuffer, txLen);
    	txPos = txLen = 0;
    }
}*/

void stkSetRxChar(unsigned char c) {
	//DEBUGSTR("stkSetRxChar\n");
    if(timerLongTimeoutOccurred()) {
        rxPos = 0;
    }

    if(rxPos == 0){     /* only accept STX as the first character */
        if(c == STK_STX)
            rxBuffer[rxPos++] = c;
    }else{
        if(rxPos < BUFFER_SIZE){
            rxBuffer[rxPos++] = c;
            if(rxPos == 4){     /* do we have length byte? */
                rxLen.bytes[0] = rxBuffer[3];
                rxLen.bytes[1] = rxBuffer[2];
                rxLen.word += 6;
                if(rxLen.word > BUFFER_SIZE){    /* illegal length */
                    rxPos = 0;      /* reset state */
                }
            }else if(rxPos == 5){   /* check whether this is the token byte */
                if(c != STK_TOKEN){
                    rxPos = 0;  /* reset state */
                }
            }else if(rxPos > 4 && rxPos == rxLen.word){ /* message is complete */
                unsigned char sum = 0;
                unsigned char *p = rxBuffer;
                while(rxPos){ /* decrement rxPos down to 0 -> reset state */
                    sum ^= *p++;
                    rxPos--;
                }
                if(sum == 0){   /* check sum is correct, evaluate rx message */
                    rxBlockAvailable = 1;
                }else{          /* checksum error */
                    txBuffer[STK_TXMSG_START] = STK_ANSWER_CKSUM_ERROR;
                    txBuffer[STK_TXMSG_START + 1] = STK_ANSWER_CKSUM_ERROR;
                    stkSetTxMessage(2);
                }
            }
        } else {  /* overflow */
        	DEBUGOUT("OK overflow: %d\n", rxPos);
            rxPos = 0;  /* reset state */
        }
    }
    timerSetupLongTimeout(RX_TIMEOUT);
}

int stkGetTxCount(void) {
    return txLen - txPos;
}

int stkGetTxByte(void) {
	unsigned char   c;

    if(txLen == 0){
        return -1;
    }
    c = txBuffer[txPos++];
    if(txPos >= txLen){         /* transmit ready */
        txPos = txLen = 0;
    }
    return c;
}

void stkPoll(void) {
    if(rxBlockAvailable){
        rxBlockAvailable = 0;
        stkEvaluateRxMessage();
    }
}

static unsigned char ispBlockTransfer(unsigned char *block, unsigned char len)
{
	unsigned char cnt, shift = 0, delay = ispClockDelay;

	NVIC_DisableIRQ(USB_IRQn);
    while(len--){   /* len may be 0 */
        cnt = 8;
        shift = *block++;
        do{
            if(shift & 0x80){
            	GPIO_SET(SOFTSPI_GPIO_MOSI);
            } else {
            	GPIO_CLR(SOFTSPI_GPIO_MOSI);
            }
            //delay_us(delay);
            delay_ms(1);
            GPIO_SET(SOFTSPI_GPIO_SCK);
            //GPIO_SCK_SET;

            shift <<= 1;
            if(GPIO_GET(SOFTSPI_GPIO_MISO)){
            //if(GPIO_MISO_IS_SET){
            	shift |= 1;
            }
            delay_ms(1);
            GPIO_CLR(SOFTSPI_GPIO_SCK);
        }while(--cnt);
    }
    NVIC_EnableIRQ(USB_IRQn);
    return shift;
}

#define TIMER_TICK_US 5
static void ispAttachToDevice(unsigned char stk500Delay, unsigned char stabDelay) {
	DEBUGSTR("ispAttachToDevice\n");
	//Jumper is set -> request clock below 8 kHz
    if (1){
    	//140 us -> 7.14 kHz clock rate.
    	ispClockDelay = (unsigned char)(70/TIMER_TICK_US);
    }else if(stk500Delay == 0){ /* 1.8 MHz nominal */
        ispClockDelay = 0;
    }else if(stk500Delay == 1){ /* 460 kHz nominal */
        ispClockDelay = 0;
    }else if(stk500Delay == 2){ /* 115 kHz nominal */
        ispClockDelay = 1;
    }else if(stk500Delay == 3){ /* 58 kHz nominal */
        ispClockDelay = 2;
    }else{
        /* from STK500v2 spec: stk500Delay = 1/(24 * SCK / 7.37 MHz) - 10/12
         * definition of ispClockDelay = 1 + 1/(SCK/MHz * 2 * TIMER_TICK_US)
         * ispClockDelay = 1 + (stk500Delay + 10/12) * 12 / (TIMER_TICK_US * 7.37)
         */
#if F_CPU > 14000000L    /* ~ 16 MHz */
        ispClockDelay = (stk500Delay + 1)/2 - (stk500Delay + 1)/8 + (stk500Delay + 1)/32;
#else   /* must be 12 MHz */
        ispClockDelay = (stk500Delay + 1)/4 + (stk500Delay + 1)/16;
#endif
    }

    softspi_init();
    //hardspi_init();
    //Chip_SPI_SetClockCounter(LPC_SPI, 8);
    GPIO_OUT(STK500_GPIO_RST);
    GPIO_MUX(STK500_GPIO_RST);

   // delay_ms(stabDelay);
    //timerTicksDelay(ispClockDelay); /* stabDelay may have been 0 */
    /* We now need to give a positive pulse on RESET since we can't guarantee
     * that SCK was low during power up (according to instructions in Atmel's
     * data sheets).
     */

    GPIO_SET(STK500_GPIO_RST);
    delay_ms(ispClockDelay);
    GPIO_CLR(STK500_GPIO_RST);  /* give a positive RESET pulse */
    delay_ms(ispClockDelay);
    //timerTicksDelay(ispClockDelay);

}

static void ispDetachFromDevice(unsigned char removeResetDelay)
{
	DEBUGSTR("ispDetachFromDevice\n");
	GPIO_SET(STK500_GPIO_RST);
	delay_ms(removeResetDelay);
}

unsigned char ispEnterProgmode(stkEnterProgIsp_t *param) {
	DEBUGSTR("ispEnterProgmode\n");
	unsigned char   i, rval;

    ispAttachToDevice(stkParam.s.sckDuration, param->stabDelay);
    delay_ms(param->cmdExeDelay);
    /* we want for(i = param->synchLoops; i--;), but avrdude sends synchLoops == 0 */
    for(i = 32; i--;){
        //wdt_reset();
        rval = ispBlockTransfer(param->cmd, param->pollIndex);
        if(param->pollIndex < 4)
            ispBlockTransfer(param->cmd + param->pollIndex, 4 - param->pollIndex);
        if(rval == param->pollValue){   /* success: we are in sync */
            return STK_STATUS_CMD_OK;
        }

        /* insert one clock pulse and try again: */
        /*  PORT_PIN_SET(HWPIN_ISP_SCK);
        timerTicksDelay(ispClockDelay);
        PORT_PIN_CLR(HWPIN_ISP_SCK);
        timerTicksDelay(ispClockDelay);*/

        GPIO_SET(SOFTSPI_GPIO_SCK);
        delay_ms(ispClockDelay);
        //delay_ms(1);
        GPIO_CLR(SOFTSPI_GPIO_SCK);
    }

    ispDetachFromDevice(0);
    return STK_STATUS_CMD_FAILED;   /* failure */
}

void ispLeaveProgmode(stkLeaveProgIsp_t *param) {
	DEBUGSTR("ispLeaveProgmode\n");
    ispDetachFromDevice(param->preDelay);
    delay_ms(param->postDelay);
}

static unsigned char deviceIsBusy(void) {
	DEBUGSTR("deviceIsBusy\n");
    cmdBuffer[0] = 0xf0;
    cmdBuffer[1] = 0;
    return ispBlockTransfer(cmdBuffer, 4) & 1;
}

static unsigned char waitUntilReady(unsigned char msTimeout) {
	//delay_ms(10);
	DEBUGSTR("waitUntilReady\n");
    timerSetupTimeout(msTimeout);
    while(deviceIsBusy()){
        if(timerTimeoutOccurred())
            return STK_STATUS_RDY_BSY_TOUT;
    }

    return STK_STATUS_CMD_OK;
}

unsigned char ispChipErase(stkChipEraseIsp_t *param) {
	DEBUGSTR("ispChipErase\n");
	unsigned char maxDelay = param->eraseDelay;
	unsigned char rval = STK_STATUS_CMD_OK;

    ispBlockTransfer(param->cmd, 4);
    if(param->pollMethod != 0){
        if(maxDelay < 10)   /* allow at least 10 ms */
            maxDelay = 10;
        rval = waitUntilReady(maxDelay);
    }else{
        delay_ms(maxDelay);
    }
    return rval;
}

unsigned char ispProgramMemory(stkProgramFlashIsp_t *param, unsigned char isEeprom) {
	DEBUGSTR("ispProgramMemory\n");
	utilWord_t  numBytes;
	unsigned char       rval = STK_STATUS_CMD_OK;
	unsigned char       valuePollingMask, rdyPollingMask;
	uint8_t        i;

    numBytes.bytes[1] = param->numBytes[0];
    numBytes.bytes[0] = param->numBytes[1];
    if(param->mode & 1){    /* page mode */
        valuePollingMask = 0x20;
        rdyPollingMask = 0x40;
    }else{                  /* word mode */
        valuePollingMask = 4;
        rdyPollingMask = 8;
    }
    if(!isEeprom && stkAddress.bytes[3] & 0x80){
        cmdBuffer[0] = 0x4d;    /* load extended address */
        cmdBuffer[1] = 0x00;
        cmdBuffer[2] = stkAddress.bytes[2];
        cmdBuffer[3] = 0x00;
        ispBlockTransfer(cmdBuffer, 4);
    }
    for(i = 0; rval == STK_STATUS_CMD_OK && i < numBytes.word; i++){
        unsigned char x;
        //wdt_reset();
        cmdBuffer[1] = stkAddress.bytes[1];
        cmdBuffer[2] = stkAddress.bytes[0];
        cmdBuffer[3] = param->data[i];
        x = param->cmd[0];
        if(!isEeprom){
            x &= ~0x08;
            if((unsigned char)i & 1){
                x |= 0x08;
                stkIncrementAddress();
            }
        }else{
            stkIncrementAddress();
        }
        cmdBuffer[0] = x;

        ispBlockTransfer(cmdBuffer, 4);
        if(param->mode & 1){            /* is page mode */
            if(i < numBytes.word - 1 || !(param->mode & 0x80))
                continue;               /* not last byte written */
            cmdBuffer[0] = param->cmd[1];     /* write program memory page */
            ispBlockTransfer(cmdBuffer, 4);
        }
        /* poll for ready after each byte (word mode) or page (page mode) */
        if(param->mode & valuePollingMask){ /* value polling */
            unsigned char d = param->data[i];
            if(d == param->poll[0] || (isEeprom && d == param->poll[1])){   /* must use timed polling */
                delay_ms(param->delay);
            }else{
                unsigned char x = param->cmd[2];     /* read memory */
                if(!isEeprom){
                    x &= ~0x08;
                    if((unsigned char)i & 1){
                        x |= 0x08;
                    }
                }
                cmdBuffer[0] = x;
                timerSetupTimeout(param->delay);
                while(ispBlockTransfer(cmdBuffer, 4) != d){
                    if(timerTimeoutOccurred()){
                        rval = STK_STATUS_CMD_TOUT;
                        break;
                    }
                }
            }
        }else if(param->mode & rdyPollingMask){ /* rdy/bsy polling */
            rval = waitUntilReady(param->delay);
        }else{                          /* must be timed delay */
            delay_ms(param->delay);
        }
    }
    return rval;
}

uint8_t ispReadMemory(stkReadFlashIsp_t *param, stkReadFlashIspResult_t *result, unsigned char isEeprom) {
	DEBUGSTR("ispReadMemory\n");
	utilWord_t  numBytes;
	unsigned char       *p, cmd0;
	uint8_t        i;

    cmdBuffer[3] = 0;
    if(!isEeprom && stkAddress.bytes[3] & 0x80){
        cmdBuffer[0] = 0x4d;    /* load extended address */
        cmdBuffer[1] = 0x00;
        cmdBuffer[2] = stkAddress.bytes[2];
        ispBlockTransfer(cmdBuffer, 4);
    }
    numBytes.bytes[1] = param->numBytes[0];
    numBytes.bytes[0] = param->numBytes[1];
    p = result->data;
    result->status1 = STK_STATUS_CMD_OK;
    cmd0 = param->cmd;
    DEBUGOUT("numBytes.word: %d 4\n" , numBytes.word);
    for(i = 0; i < numBytes.word; i++){
        //wdt_reset();

        cmdBuffer[1] = stkAddress.bytes[1];
        cmdBuffer[2] = stkAddress.bytes[0];
        if(!isEeprom){
            if((unsigned char)i & 1){
                cmd0 |= 0x08;
                stkIncrementAddress();
            }else{
                cmd0 &= ~0x08;
            }
        }else{
            stkIncrementAddress();
        }
        cmdBuffer[0] = cmd0;
        *p++ = ispBlockTransfer(cmdBuffer, 4);
        DEBUGOUT("ispReadMemory: %d/%d 4\n" , numBytes.word,i);
    }

    *p = STK_STATUS_CMD_OK; /* status2 */
    DEBUGSTR("ispReadMemory END\n");
    return numBytes.word + 2;
}

unsigned char ispProgramFuse(stkProgramFuseIsp_t *param) {
	DEBUGSTR("ispProgramFuse\n");
    ispBlockTransfer(param->cmd, 4);
    return STK_STATUS_CMD_OK;
}

unsigned char ispReadFuse(stkReadFuseIsp_t *param) {
	DEBUGSTR("ispReadFuse\n");
	unsigned char rval;

    rval = ispBlockTransfer(param->cmd, param->retAddr);
    if(param->retAddr < 4)
        ispBlockTransfer(param->cmd + param->retAddr, 4 - param->retAddr);
    return rval;
}

uint8_t ispMulti(stkMultiIsp_t *param, stkMultiIspResult_t *result) {
	DEBUGSTR("ispMulti\n");
	unsigned char cnt1, i, *p;

    cnt1 = param->numTx;
    if(cnt1 > param->rxStartAddr)
        cnt1 = param->rxStartAddr;
    ispBlockTransfer(param->txData, cnt1);

    p = result->rxData;
    for(i = 0; i < param->numTx - cnt1; i++){
        unsigned char b = ispBlockTransfer(&param->txData[cnt1] + i, 1);
        if(i < param->numRx)
            *p++ = b;
        //wdt_reset();
    }

    for(; i < param->numRx; i++){
        cmdBuffer[0] = 0;
        *p++ = ispBlockTransfer(cmdBuffer, 1);
        //wdt_reset();
    }
    *p = result->status1 = STK_STATUS_CMD_OK;
    return (uint8_t)param->numRx + 2;
}
