#include "softspi.h"
#include "gpiox.h"
#include "config.h"
#include "timer.h"

void softspi_init(void){
    GPIO_OUT(SOFTSPI_GPIO_MOSI);
    GPIO_OUT(SOFTSPI_GPIO_SCK);
    GPIO_INP(SOFTSPI_GPIO_MISO);
    #ifdef __ARM_ARCH
    GPIO_MUX(SOFTSPI_GPIO_MOSI);
    GPIO_MUX(SOFTSPI_GPIO_SCK);
    GPIO_MUX(SOFTSPI_GPIO_MISO);
    #endif
}

uint8_t softspi_txrx_byte(uint8_t shift){
    uint8_t cnt = 8;
    do{
        if(shift & 0x80){
            GPIO_SET(SOFTSPI_GPIO_MOSI);
        } else {
            GPIO_CLR(SOFTSPI_GPIO_MOSI);
        }
        //delay_ms(1);
        GPIO_SET(SOFTSPI_GPIO_SCK);
        shift <<= 1;
        if(GPIO_GET(SOFTSPI_GPIO_MISO)){
            shift |= 1;
        }
        //delay_ms(1);
        GPIO_CLR(SOFTSPI_GPIO_SCK);
    } while(--cnt);
    return shift;
}

uint8_t softspi_txrx_buff(uint8_t *block, uint8_t len){
    uint8_t cnt, shift = 0;
    while(len--){   /* len may be 0 */
        cnt = 8;
        shift = *block++;
        do{
            if(shift & 0x80){
                GPIO_SET(SOFTSPI_GPIO_MOSI);
            } else {
                GPIO_CLR(SOFTSPI_GPIO_MOSI);
            }
            delay_ms(1);
            GPIO_SET(SOFTSPI_GPIO_SCK);
            shift <<= 1;
            if(GPIO_GET(SOFTSPI_GPIO_MISO)){
                shift |= 1;
            }
            delay_ms(1);
            GPIO_CLR(SOFTSPI_GPIO_SCK);
        } while(--cnt);
    }
    return shift;
}
