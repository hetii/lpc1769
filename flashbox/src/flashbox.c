#include <cr_section_macros.h>
#include "board.h"
#include "cdc_vcom.h"
#include "timer.h"
#include "stk500.h"

int main(void) {
	static uint8_t g_rxBuff[256];
    int  c;
	uint32_t i, rdCnt;
	unsigned char USBbuff[USB_FS_MAX_BULK_PACKET];
    unsigned char k = 0;

	SystemCoreClockUpdate();
    Board_Init();
    SysTickInit();
    VcomInit();

	while (1) {
		stkPoll();
		rdCnt = vcom_bread(&g_rxBuff[0], 256);

		if (rdCnt>0){
			DEBUGOUT("OK rdCnt: %d\n", rdCnt);
			for(i = 0; i < rdCnt; i++){
				stkSetRxChar(g_rxBuff[i]);
			}
		}
		//stkEmitTxBytes();

		while((c = stkGetTxByte()) >= 0){
			USBbuff[k++] = c;
			if (k > USB_FS_MAX_BULK_PACKET){
				vcom_write(USBbuff, k-1);
				k = 0;
			}
		}

		if(k > 0){
			vcom_write(USBbuff, k);
			k = 0;
		}
	}
	/* Sleep until next IRQ happens */
	//__WFI();
    return 0 ;
}
