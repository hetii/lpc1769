#include <lpc_types.h>
#include "config.h"
#include "board.h"

/*
LPC_GPIO_T *GPIO_GetPointer(uint8_t portNum){
    LPC_GPIO_T *pGPIO = NULL;

    switch (portNum) {
    case 0:
        pGPIO = LPC_GPIO;
        break;
    case 1:
        pGPIO = LPC_GPIO1;
        break;
    case 2:
        pGPIO = LPC_GPIO2;
        break;
    case 3:
        pGPIO = LPC_GPIO3;
        break;
    case 4:
        pGPIO = LPC_GPIO4;
        break;
    default:
        break;
    }
    return pGPIO;
}

inline bool GPIOGetValue (uint8_t portNum, uint8_t portPin){
    LPC_GPIO_T *pGPIO = GPIO_GetPointer(portNum);
    return Chip_GPIO_GetPinState(pGPIO, portNum, portPin);
}

void GPIOSetValue(uint8_t portNum, uint8_t portPin, uint8_t value){
    LPC_GPIO_T *pGPIO = GPIO_GetPointer(portNum);
    Chip_GPIO_SetPinState(pGPIO, portNum, portPin, value);
}

void GPIO_ToogleXtimes(uint8_t portNum, uint8_t portPin, uint8_t times){
    LPC_GPIO_T *pGPIO = GPIO_GetPointer(portNum);
    while(times--){
        delay_ms(200);
        Chip_GPIO_SetPinState(pGPIO, portNum, portPin, 1);
        delay_ms(200);
        Chip_GPIO_SetPinState(pGPIO, portNum, portPin, 0);
    }
}

void GPIOSetDir(uint32_t portNum, uint32_t bitPosi, uint32_t dir){
    LPC_GPIO_T *pGPIO = GPIO_GetPointer(portNum);
    Chip_GPIO_SetPinDIR(pGPIO, portNum, bitPosi, dir);
}
*/

