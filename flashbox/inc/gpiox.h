#ifndef DRV_GPIO_H_
#define DRV_GPIO_H_

/* Global helpers */
#define GPIO_SET(x) _XS(x) //Set pin High.
#define GPIO_CLR(x) _XC(x) //Set pin Low.
#define GPIO_GET(x) _XG(x) //Get pin state, True/High, False/Low.
#define GPIO_TOG(x) _XT(x) //Toggle pin state.
#define GPIO_OUT(x) _XO(x) //Set pin as output.
#define GPIO_INP(x) _XI(x) //Set pin as input.
#define GPIO_MUX(x) _XM(x) //Set MUX as inact/func0.
#define GPIO_RST(x) _XC(x);uint8_t x=255;while(x--);_XS(x);

/* Architecture helpers */
#ifdef __ARM_ARCH
    #include "chip.h"

    /* Alias for LPC_GPIO needed by _XS */
    #if !defined LPC_GPIO0 && defined LPC_GPIO
        #define LPC_GPIO0 LPC_GPIO
    #endif

    #define _XS(x,y) Chip_GPIO_SetPinOutHigh(LPC_GPIO,x,y)
    #define _XC(x,y) Chip_GPIO_SetPinOutLow(LPC_GPIO,x,y)
    #define _XG(x,y) Chip_GPIO_GetPinState(LPC_GPIO,x,y)
    #define _XT(x,y) Chip_GPIO_SetPinToggle(LPC_GPIO,x,y)
    #define _XO(x,y) Chip_GPIO_SetPinDIROutput(LPC_GPIO,x,y)
    #define _XI(x,y) Chip_GPIO_SetPinDIRInput(LPC_GPIO,x,y)
/*

    #define _XS(x,y) Chip_GPIO_SetPinOutHigh(LPC_GPIO##x,x,y)
    #define _XC(x,y) Chip_GPIO_SetPinOutLow(LPC_GPIO##x,x,y)
    #define _XG(x,y) Chip_GPIO_GetPinState(LPC_GPIO##x,x,y)
    #define _XT(x,y) Chip_GPIO_SetPinToggle(LPC_GPIO##x,x,y)
    #define _XO(x,y) Chip_GPIO_SetPinDIROutput(LPC_GPIO##x,x,y)
    #define _XI(x,y) Chip_GPIO_SetPinDIRInput(LPC_GPIO##x,x,y)
*/

    #define _XM(x,y) Chip_IOCON_PinMux(LPC_IOCON, x, y,  IOCON_MODE_INACT, IOCON_FUNC0);
#elif defined __AVR
    #define _XS(x,y) PORT##x |= (1<<y)
    #define _XC(x,y) PORT##x &= ~(1<<y) // Pin n goes low
    #define _XG(x,y) ((PIN##x & (1<<y)) != 0)
    #define _XT(x,y) PORT##x ^= (1<<y)
    #define _XO(x,y) DDR##x |= (1<<y)
    #define _XI(x,y) DDR##x &= ~(1<<y)
#endif

inline bool GPIOGetValue (uint8_t portNum, uint8_t portPin);
void GPIOSetValue(uint8_t portNum, uint8_t portPin, uint8_t value);
void GPIOToogleXtimes(uint8_t portNum, uint8_t portPin, uint8_t times);
void GPIOSetDir(uint32_t portNum, uint32_t bitPosi, uint32_t dir);

#endif /* DRV_GPIOX_H_ */
